extends CanvasLayer

# Debug overlay by Gonkee - full tutorial https://youtu.be/8Us2cteHbbo
var help = false

onready var crosshair = $CrosshairContainer/Crosshair

# MenuButtons with PopupMenus
onready var game_menu = $MarginContainer/VBoxContainer/TopHBoxContainer/GameButton
onready var level_menu = $MarginContainer/VBoxContainer/TopHBoxContainer/LevelButton
onready var player_menu = $MarginContainer/VBoxContainer/TopHBoxContainer/PlayerButton
# PopupMenus for submenus
var music_submenu = PopupMenu.new()
var speed_submenu = PopupMenu.new()
var cam_submenu = PopupMenu.new()
var fov_submenu = PopupMenu.new()
var fog_submenu = PopupMenu.new()

onready var debug_label = $MarginContainer/VBoxContainer/MiddleMarginContainer/HBoxContainer/Debug
var debug_text_default
onready var debug_container = $Debug/VBoxContainer
onready var event_text = debug_container.get_node("Event")

onready var left_touch_joystick = $MarginContainer/VBoxContainer/BottomHBoxContainer/LeftTouch/Joystick
onready var right_touch_joystick = $MarginContainer/VBoxContainer/BottomHBoxContainer/RightTouch/Joystick

onready var about_dialog = $About_WindowDialog
onready var file_dialog = $FileDialog

onready var dialogue_container = $Dialogue
onready var dialogue_big = dialogue_container.get_node("Big")
onready var dialogue_small = dialogue_container.get_node("Small")

var stats = []
#onready var env = $"/root/Level/WorldEnvironment".environment

export var details = 1
onready var materials = [
	# Terrain
	#$"../Platform".material,
	#$"../Terrain/Canyon".mesh.surface_get_material(0),
	#$"../Terrain/Canyons".mesh.surface_get_material(0),
	#$"../Terrain/Desert".mesh.surface_get_material(0),
	#$"../Terrain/Riverland".mesh.surface_get_material(0),
	#$"../Terrain/Flatstones".mesh.surface_get_material(0),
	#$"../Terrain/Lakes1/Lakes1".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/DragonCastle".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/Dungeon".mesh.surface_get_material(0),
	#$"../Terrain/Hills".mesh.surface_get_material(0),
	# Animals
	#$"../Animals/DragonTutu/Armature/Skeleton/Tutu".mesh.surface_get_material(0),
	]

func _ready():
	init_top_menu()

	debug_text_default = debug_label.text
	# env.fog_enabled = true
	call_deferred("when_ready")

func when_ready():
	var time = OS.get_time()
	var str_time = str(time.hour, ":", time.minute, ":", time.second)
	set_event_text("Welcome in Foraxx (" + OS.get_name() + ") at " + str_time + "\n\nUse WASD keys to move around.", 5.0)

func init_top_menu():
	var popup

	# Game Menu
	music_submenu.set_name("music_submenu")
	music_submenu.add_item("Play/Stop (.)")
	music_submenu.add_item("Next Track (,)")
	music_submenu.add_item("Toggle Shuffle")
	music_submenu.connect("id_pressed", self, "_on_music_submenu_pressed")

	popup = game_menu.get_popup()
	popup.add_item("Back to Start (Backspace)")
	popup.add_item("Return to Lobby (L)")
	popup.add_separator()
	popup.add_item("Save Game (F2)")
	popup.add_item("Load Game (F3)")
	popup.add_separator()
	popup.add_child(music_submenu)
	popup.add_submenu_item("Music", "music_submenu")
	popup.add_separator()
	popup.add_item("Controls (F1)")
	popup.add_item("Fullscreen (F11)")
	popup.add_item("Virtual Joysticks")
	popup.add_item("AR/VR (V)")
	popup.add_item("Toggle Debug (F12)")
	popup.add_separator()
	popup.add_item("About Foraxx")
	popup.add_item("Quit")
	popup.connect("id_pressed", self, "_on_game_menu_pressed")

	# Level Menu
	popup = level_menu.get_popup()
	for level in LevelManager.levels.keys():
		popup.add_item(LevelManager.levels[level]["name"])
	popup.add_separator()
	popup.add_item("Open Custom Level")
	popup.add_separator()
	popup.add_item("Random Environment (F7)")
	popup.add_separator()
	popup.add_item("Toggle Procedural Terrain")
	popup.connect("id_pressed", self, "_on_level_menu_pressed")

	# Player Menu
	popup = player_menu.get_popup()

	cam_submenu.set_name("cam_submenu")
	cam_submenu.add_item("Perspective Projection (Num5)")
	cam_submenu.add_item("Orthogonal Projection (Num5)")
	cam_submenu.add_separator()
	cam_submenu.add_item("First-person View (Ctrl+Num5)")
	cam_submenu.add_item("Third-person View (Ctrl+Num5)")
	cam_submenu.add_item("Top View (Num7)")
	cam_submenu.add_item("Bottom View (Ctrl+Num7)")
	cam_submenu.add_item("Front View (Num1)")
	cam_submenu.add_item("Back View (Ctrl+Num1)")
	cam_submenu.add_item("Right View (Num3)")
	cam_submenu.add_item("Left View (Ctrl+Num3)")
	cam_submenu.add_separator()

	fov_submenu.set_name("fov_submenu")
	fov_submenu.add_item("-FOV / Zoom In (ScrollUp)")
	fov_submenu.add_item("+FOV / Zoom Out (ScrollDown)")
	fov_submenu.add_item("Reset FOV to 90deg (/)")
	fov_submenu.add_separator()
	fov_submenu.add_item("FOV 120deg")
	fov_submenu.add_item("FOV 100deg")
	fov_submenu.add_item("FOV 90deg")
	fov_submenu.add_item("FOV 80deg")
	fov_submenu.add_item("FOV 70deg")
	fov_submenu.add_item("FOV 60deg")
	fov_submenu.add_item("FOV 45deg")
	fov_submenu.add_item("FOV 30deg")
	fov_submenu.add_item("FOV 15deg")
	fov_submenu.add_item("FOV 5deg")
	fov_submenu.add_item("FOV 1deg")
	fov_submenu.connect("id_pressed", self, "_on_fov_submenu_pressed")
	cam_submenu.add_child(fov_submenu)
	cam_submenu.add_submenu_item("FOV / Zoom", "fov_submenu")
	cam_submenu.add_separator()

	cam_submenu.add_item("Toggle Fog (F10)")
	cam_submenu.add_item("-Fog ([)")
	cam_submenu.add_item("+Fog (])")
	cam_submenu.add_item("Camera Far 1")
	cam_submenu.add_item("Camera Far 10")
	cam_submenu.add_item("Camera Far 100")
	cam_submenu.add_item("Camera Far 1k")
	cam_submenu.add_item("Camera Far 10k")
	cam_submenu.add_item("Camera Far 100k")
	cam_submenu.add_item("Camera Far 1m")
	cam_submenu.connect("id_pressed", self, "_on_cam_submenu_pressed")
	popup.add_child(cam_submenu)
	popup.add_submenu_item("Camera", "cam_submenu")
	popup.add_separator()

	speed_submenu.set_name("speed_submenu")
	speed_submenu.add_item("Speed up (Num+)")
	speed_submenu.add_item("Slow down (Num-)")
	speed_submenu.add_separator()
	speed_submenu.add_item("100 (1)")
	speed_submenu.add_item("500 (2)")
	speed_submenu.add_item("2k (3)")
	speed_submenu.add_item("10k (4)")
	speed_submenu.add_item("50k (5)")
	speed_submenu.connect("id_pressed", self, "_on_speed_submenu_pressed")
	popup.add_child(speed_submenu)
	popup.add_submenu_item("Speed", "speed_submenu")

	popup.add_item("Enlarge (Ctrl+plus)")
	popup.add_item("Shrink (Ctrl+minus)")
	popup.add_item("Reset size (Ctrl+0)")
	popup.add_item("Fly (Tab)")
	popup.add_item("Flashlight (F)")
	popup.connect("id_pressed", self, "_on_player_menu_pressed")

func _on_game_menu_pressed(item_id):
	var item_text = game_menu.get_popup().get_item_text(item_id)
	match item_text:
		"Back to Start (Backspace)":
			LevelManager.player_return_to_start()
		"Return to Lobby (L)":
			LevelManager.goto_level("constructor")
		"Save Game (F2)":
			Settings.save()
		"Load Game (F3)":
			Settings.load()
		"Controls (F1)":
			toggle_help()
		"Fullscreen (F11)":
			OS.window_fullscreen = !OS.window_fullscreen
		"Virtual Joysticks":
			toggle_joysticks()
		"AR/VR (V)":
			Global.toggle_arvr()
		"Toggle Debug (F12)":
			Global.debug = not Global.debug
			if not Global.debug:
				debug_label.text = debug_text_default
		"About Foraxx":
			show_about()
		"Quit":
			get_tree().quit()
		_:
			printerr("Unprocessed item in Game Menu: ", item_id, " (", item_text, ")")

func _on_music_submenu_pressed(item_id):
	var item_text = music_submenu.get_item_text(item_id)
	match item_text:
		"Play/Stop (.)":
			# warning-ignore:return_value_discarded
			Music.toggle_playing()
		"Next Track (,)":
			# warning-ignore:return_value_discarded
			Music.play_next()
		"Toggle Shuffle":
			# warning-ignore:return_value_discarded
			Music.toggle_shuffle()
		_:
			printerr("Unprocessed item in Music Submenu: ", item_id, " (", item_text, ")")

func _on_level_menu_pressed(item_id):
	var item_text = level_menu.get_popup().get_item_text(item_id)
	match item_text:
		"Open Custom Level":
			file_dialog.popup()
		"Random Environment (F7)":
			LevelEnvironment.rand_environment()
		"Toggle Procedural Terrain":
			if LevelManager.current_scene.has_node("Generator"):
				var generator = LevelManager.current_scene.get_node("Generator")
				generator.set_process(!generator.is_processing())
				#generator.noise.seed = randi()
		_:
			var level_found = false
			for level in LevelManager.levels.keys():
				if (item_text == LevelManager.levels[level]["name"]):
					LevelManager.goto_level(level)
					level_found = true

			if not level_found:
				printerr("Unprocessed item in Level Menu: ", item_id, " (", item_text, ")")

func _on_player_menu_pressed(item_id):
	var item_text = player_menu.get_popup().get_item_text(item_id)
	var player = Global.player
	match item_text:
		"Enlarge (Ctrl+plus)":
			player.scale_object_local(Vector3(1.3333, 1.3333, 1.3333))
		"Shrink (Ctrl+minus)":
			player.scale_object_local(Vector3(0.75, 0.75, 0.75))
		"Reset size (Ctrl+0)":
			player.set_scale(Vector3(1.0, 1.0, 1.0))
		"Fly (Tab)":
			if player.fly_mode == 1:
				#print("falling")
				player.fly_mode = 0
				player.gravity = -60
			# Switch to Keyboard Fly Mode
			else:
				#print("fly with keyboard")
				player.fly_mode = 1
				player.gravity = 0
				player.velocity.y = 0
		"Flashlight (F)":
			Global.player.flashlight.visible = not Global.player.flashlight.visible
		_:
			printerr("Unprocessed item in Player Menu: ", item_id, " (", item_text, ")")

func _on_speed_submenu_pressed(item_id):
	var item_text = speed_submenu.get_item_text(item_id)
	var player = Global.player
	match item_text:
		"Speed up (Num+)":
			# Multiplier should be based on interaction in UI vs. keyboard
			player.speed *= 2.0
		"Slow down (Num-)":
			player.speed = max(1, player.speed/2.0)
		"100":
			player.speed = 100
		"500":
			player.speed = 500
		"2k":
			player.speed = 2000
		"10k":
			player.speed = 10000
		"50k":
			player.speed = 50000
		_:
			printerr("Unprocessed item in Speed Submenu: ", item_id, " (", item_text, ")")

func _on_cam_submenu_pressed(item_id):
	var item_text = cam_submenu.get_item_text(item_id)
	var player = Global.player
	match item_text:
		"Perspective Projection (Num5)":
			player.cam_perspective()
		"Orthogonal Projection (Num5)":
			player.cam_orthogonal()

		"First-person View (Ctrl+Num5)":
			player.cam_1st()
			player.cam_perspective()
		"Third-person View (Ctrl+Num5)":
			player.cam_3rd()
			player.cam_perspective()
		"Top View (Num7)":
			player.cam_top_view()
		"Bottom View (Ctrl+Num7)":
			player.cam_bottom_view()
		"Front View (Num1)":
			player.cam_front_view()
		"Back View (Ctrl+Num1)":
			player.cam_back_view()
		"Right View (Num3)":
			player.cam_right_view()
		"Left View (Ctrl+Num3)":
			player.cam_left_view()

		"Toggle Fog (F10)":
			LevelEnvironment.env.environment.fog_enabled = !LevelEnvironment.env.environment.fog_enabled
		"-Fog ([)":
			var cam = player.cam
			cam.far = max(10.0, cam.far / 1.5)
			var environment = LevelEnvironment.env.environment
			environment.fog_depth_end = cam.far
			environment.fog_depth_begin = environment.fog_depth_end / 2
		"+Fog (])":
			var cam = player.cam
			cam.far = min(2000000, cam.far * 1.5)
			var environment = LevelEnvironment.env.environment
			environment.fog_depth_end = cam.far
			environment.fog_depth_begin = environment.fog_depth_end / 2
		"Camera Far 1":
			player.cam.far = 1
		"Camera Far 10":
			player.cam.far = 10
		"Camera Far 100":
			player.cam.far = 100
		"Camera Far 1k":
			player.cam.far = 1e3
		"Camera Far 10k":
			player.cam.far = 1e4
		"Camera Far 100k":
			player.cam.far = 1e5
		"Camera Far 1m":
			player.cam.far = 1e6
		_:
			printerr("Unprocessed item in Camera Submenu: ", item_id, " (", item_text, ")")

func _on_fov_submenu_pressed(item_id):
	var item_text = fov_submenu.get_item_text(item_id)
	var player = Global.player
	match item_text:
		"-FOV / Zoom In (ScrollUp)":
			player.cam.fov /= 1.5
		"+FOV / Zoom Out (ScrollDown)":
			player.cam.fov *= 1.5
		"Reset FOV (/)":
			player.fov_reset()
		"FOV 120deg":
			player.cam.fov = 120
		"FOV 100deg":
			player.cam.fov = 100
		"FOV 90deg":
			player.cam.fov = 90
		"FOV 80deg":
			player.cam.fov = 80
		"FOV 70deg":
			player.cam.fov = 70
		"FOV 60deg":
			player.cam.fov = 60
		"FOV 45deg":
			player.cam.fov = 45
		"FOV 30deg":
			player.cam.fov = 30
		"FOV 15deg":
			player.cam.fov = 15
		"FOV 5deg":
			player.cam.fov = 5
		"FOV 1deg":
			player.cam.fov = 1
		_:
			printerr("Unprocessed item in FOV Submenu: ", item_id, " (", item_text, ")")

func _on_Button_Quests_pressed():
	toggle_quests()

func _on_Button_Inventory_pressed():
	toggle_inventory()

func show_top_menu():
	$MarginContainer/VBoxContainer/TopHBoxContainer.show()

func hide_top_menu():
	$MarginContainer/VBoxContainer/TopHBoxContainer.hide()

func toggle_top_menu() -> bool:
	if $MarginContainer/VBoxContainer/TopHBoxContainer.is_visible_in_tree():
		hide_top_menu()
		$Dialog/PopupMenu.hide()
		return true
	else:
		show_top_menu()
		$Dialog/PopupMenu.popup()
		return false

func show_joysticks():
	$MarginContainer/VBoxContainer/BottomHBoxContainer.show()

func hide_joysticks():
	$MarginContainer/VBoxContainer/BottomHBoxContainer.hide()

func toggle_joysticks():
	if $MarginContainer/VBoxContainer/BottomHBoxContainer.visible:
		hide_joysticks()
	else:
		show_joysticks()

func enable_touch_control():
	show_top_menu()
	show_joysticks()

func disable_touch_control():
	hide_top_menu();
	hide_joysticks()

func add_stat(stat_name, object, stat_ref, is_method):
	stats.append([stat_name, object, stat_ref, is_method])

# warning-ignore:unused_argument
func _process(delta) -> void:
	if Global.debug:
		show_debug()

func _input(_event) -> void:
	if _event is InputEventKey and _event.is_pressed():
		if _event.is_action("ui_dialog_toggle"):
			HUD.hide_event_text()
			HUD.dialogue_close()
			HUD.dialogue_big_close()

		elif _event.is_action("ui_help"):
			toggle_help()

		elif _event.is_action("ui_toggle_fog"):
			LevelEnvironment.env.environment.fog_enabled = !LevelEnvironment.env.environment.fog_enabled

		elif _event.is_action("ui_details"):
			# Switch level of details
			details += 1
			if (details > 2):
				details = 0

			# Apply settings for current level of details
			if (details == 0):
				for material in materials:
					# DiffuseMode: DIFFUSE_BURLEY=0, DIFFUSE_LAMBERT=1, DIFFUSE_LAMBERT_WRAP=2, DIFFUSE_OREN_NAYAR=3, DIFFUSE_TOON=4
					#material.set_diffuse_mode(SpatialMaterial.DIFFUSE_BURLEY)

					# Flags: FLAG_UNSHADED=0, FLAG_USE_VERTEX_LIGHTING=1, FLAG_DISABLE_DEPTH_TEST=2, FLAG_ALBEDO_FROM_VERTEX_COLOR=3,
					# FLAG_SRGB_VERTEX_COLOR=4, FLAG_USE_POINT_SIZE=5, FLAG_FIXED_SIZE=6, FLAG_BILLBOARD_KEEP_SCALE=7,
					# FLAG_UV1_USE_TRIPLANAR=8, FLAG_UV2_USE_TRIPLANAR=9, FLAG_TRIPLANAR_USE_WORLD=10, FLAG_AO_ON_UV2=11,
					# FLAG_EMISSION_ON_UV2=12, FLAG_USE_ALPHA_SCISSOR=13, FLAG_ALBEDO_TEXTURE_FORCE_SRGB=14,
					# FLAG_DONT_RECEIVE_SHADOWS=15, FLAG_ENSURE_CORRECT_NORMALS=16, FLAG_DISABLE_AMBIENT_LIGHT=17,
					# FLAG_USE_SHADOW_TO_OPACITY=18
					material.set_flag(SpatialMaterial.FLAG_UNSHADED, 1)
					material.set_flag(SpatialMaterial.FLAG_USE_VERTEX_LIGHTING, 1)

					# SpecularMode: SPECULAR_SCHLICK_GGX=0, SPECULAR_BLINN=1, SPECULAR_PHONG=2, SPECULAR_TOON=3, SPECULAR_DISABLED=4
					material.set_specular_mode(SpatialMaterial.SPECULAR_DISABLED)

					#  BlendMode: BLEND_MODE_MIX=0, BLEND_MODE_ADD=1, BLEND_MODE_SUB=2, BLEND_MODE_MUL=3
					#material.set_blend_mode(SpatialMaterial.BLEND_MODE_MIX)

					# CullMode: CULL_BACK=0, CULL_FRONT=1, CULL_DISABLED=2
					#material.set_cull_mode(SpatialMaterial.CULL_DISABLED)

					# TEXTURE_ALBEDO=0, TEXTURE_METALLIC=1, TEXTURE_ROUGHNESS=2, TEXTURE_EMISSION=3, TEXTURE_NORMAL=4
					# TEXTURE_RIM=5, TEXTURE_CLEARCOAT=6, TEXTURE_FLOWMAP=7, TEXTURE_AMBIENT_OCCLUSION=8, TEXTURE_DEPTH=9
					# TEXTURE_SUBSURFACE_SCATTERING=10, TEXTURE_TRANSMISSION=11, TEXTURE_REFRACTION=12, TEXTURE_DETAIL_MASK=13
					# TEXTURE_DETAIL_ALBEDO=14, TEXTURE_DETAIL_NORMAL = 15

			elif (details == 1):
				for material in materials:
					material.set_flag(SpatialMaterial.FLAG_UNSHADED, 0)

			elif (details == 2):
				for material in materials:
					material.set_specular_mode(SpatialMaterial.SPECULAR_SCHLICK_GGX)
					material.set_flag(SpatialMaterial.FLAG_USE_VERTEX_LIGHTING, 0)

		elif _event.is_action("ui_toggle_fullscreen"):
			OS.window_fullscreen = !OS.window_fullscreen

		elif _event.is_action("ui_debug"):
			Global.debug = not Global.debug
			if not Global.debug:
				debug_label.text = debug_text_default

func set_event_text(text: String, autohide:float = 0.0) -> void:
	if (event_text):
		event_text.visible = true
		event_text.text = text
	if (autohide):
		#yield(get_tree(), "idle_frame")
		yield(get_tree().create_timer(autohide), "timeout")
		hide_event_text()

func hide_event_text() -> void:
	event_text.text = ""
	event_text.visible = false

func dialogue(text: String) -> void:
	dialogue_small.get_node("Text").bbcode_text = text
	dialogue_small.visible = true
	dialogue_container.visible = true

func dialogue_close() -> void:
	dialogue_small.visible = false
#	if (dialogue_big.visible == false):
	dialogue_container.visible = false

# warning-ignore:function_conflicts_variable
func dialogue_big(text: String) -> void:
	dialogue_big.get_node("Text").bbcode_text = text
	dialogue_big.visible = true
	dialogue_container.visible = true

func dialogue_big_close() -> void:
	dialogue_big.visible = false
#	if (dialogue_small.visible == false):
	dialogue_container.visible = false

func toggle_help() -> void:
	help = not help
	if help:
		show_help()
	else:
		dialogue_big_close()

func show_help() -> void:
	# Hide Event text
	hide_event_text()

	var info_text = """[code]Esc | U : Toggle mouse mode
1,2,3,4,5,6 : Speed presets   H : Random environment
Left click : Seed the tree    Right click : Build the house
WASD : Move                   Left, Right, Up, Down : Rotate
Space | Q : Jump              C | E : Crouch
Shift : Run (double speed)    F : Flashlight
Tab : Fly with keyboard (stop/start falling) + Space (fly up) + C|E (fly down)
Shift+Tab : Fly with mouse (stop/start falling)
+ and - : Speed up and Slow down (min 1)
Scroll with mouse : Change Camera FOV
/ : Reset Camera FOV         [ and ] : Camera Far and Fog closer and further
Backspace : Back to start     B / N / L : Previous / Next / Lobby Level
F2 / F3 : Save / Load
F5 / F6 : Switch 1st & 3rd person view / Camera projection Perspective and Ortho
F9 / F10 / F11 / F12 : Switch Details / Fog / Fullscreen / Debug
O : Hide all dialogs
. (period) / , (comma) / '/' (slash) : Music Play/Pause / Next track / Toggle shuffle

[img]res://assets/ui/keyboard.png[/img]

[img]res://assets/ui/gamepad.png[/img]
[/code]"""
	dialogue_big(info_text)

func show_debug() -> void:
	var debug_text = debug_text_default + "\n\n"

	var player = Global.player
	var cam = player.cam
	#var cam = player.get_node("Player").cam
	debug_text += str("FPS: ", Engine.get_frames_per_second()) + "\n"
	debug_text += str("Static Memory: ", String.humanize_size(OS.get_static_memory_usage())) + "\n"
	debug_text += str("Level of Details: ", details) + "\n"
	if cam.projection == Camera.PROJECTION_PERSPECTIVE:
		debug_text += str("Camera FOV: ", cam.fov, "   Far: ", cam.far) + "\n"
	else:
		debug_text += str("Camera Size: ", cam.size, ",   Far: ", cam.far) + "\n"

	debug_text += "Status: "
	if player.jumping:
		debug_text += "[jumping]"
	if player.crouching:
		debug_text += "[crouching]"
	if player.fly_mode:
		if (player.fly_mode == 1):
			debug_text += "[fly with keyboard]"
		elif (player.fly_mode == 2):
			debug_text += "[fly with mouse]"
	else:
		debug_text += "[falling]"
	debug_text += "\n"

	debug_text += str("Speed: ", player.speed) + "\n"
	debug_text += "Player Velocity [X] " + str(player.velocity.x) + " [Y] " + str(player.velocity.y) + " [Z] " + str(player.velocity.z) + "\n"
	debug_text += "Player Translation [X] " + str(player.translation.x) + " [Y] " + str(player.translation.y) + " [Z] " + str(player.translation.z) + "\n"
	debug_text += "Player Rotation [X] " + str(player.rotation.x) + " [Y] " + str(player.rotation.y) + " [Z] " + str(player.rotation.z) + "\n"
	debug_text += "Camera Rotation [X] " + str(player.cam.rotation.x) + " [Y] " + str(player.cam.rotation.y) + " [Z] " + str(player.cam.rotation.z) + "\n"

	for s in stats:
		var value = null

		if s[1] and weakref(s[1]).get_ref():
			if s[3]:
				value = s[1].call(s[2])
			else:
				value = s[1].get(s[2])
		debug_text += str(s[0], ": ", value)
		debug_text += "\n"

	debug_label.text = debug_text

func show_about() -> void:
	about_dialog.popup()

func toggle_inventory() -> void:
	pass

func toggle_quests() -> void:
	pass
