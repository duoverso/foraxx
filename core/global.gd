extends Node

export var debug = false

export(int, "Visible", "Hidden", "Captured", "Confined") var mouse_mode = Input.MOUSE_MODE_CAPTURED

var has_touch

export var start_in_arvr = false
onready var arvr_interface = ARVRServer.find_interface("Native mobile")

onready var player:KinematicBody = $Player

#func _enter_tree():
#	print(":: global.gd: _enter_tree() at path: " + get_path())

func _ready():
	#var DownloadManager = preload("res://core/net/download_manager.gd")
	#print(DownloadManager)
	DownloadManager.download_user_files()
	#get_tree().quit()

	if debug:
		HUD.debug_container.show()
	#else:
		#HUD.debug_container.hide()

	#Settings.load()

	if start_in_arvr:
		#print("start_in_arvr... interface: " + str(arvr_interface) + "; initialized: " + str(arvr_interface.initialize()))
		#HUD.debug_container.get_node("Event").text = str(ARVRServer.get_interfaces())
		start_arvr()

	var os_name = OS.get_name() # possible values: "Android", "iOS", "HTML5", "OSX", "Server", "Windows", "UWP", "X11"
	#var time = OS.get_time()
	#var str_time = str(time.hour, ":", time.minute, ":", time.second)

	HUD.dialogue_big_close()
	HUD.dialogue_close()
	#HUD.set_event_text("Welcome in Foraxx (" + OS.get_name() + ") at " + str_time + "\n\nUse WASD keys to move around.", 5.0)
	if os_name in ["Android", "BlackBerry 10", "iOS"]:
		init_mobile()
	elif os_name in ["Flash", "HTML5"]:
		init_web()
	else:
		init_desktop()
	if os_name == "Android":
		$DirectionalLight.light_energy = 0

	if OS.has_touchscreen_ui_hint():
		has_touch = true
		#init_touchscreen()
		HUD.enable_touch_control()
	else:
		has_touch = false
		HUD.disable_touch_control()

	#OS.alert("alert!")
	#OS.shell_open("https://godotengine.org")

func start_arvr():
	if arvr_interface and arvr_interface.initialize():
		if OS.has_touchscreen_ui_hint():
			has_touch = false
			HUD.disable_touch_control()
		HUD.crosshair.hide()
		HUD.debug_container.hide()
		HUD.get_node("MarginContainer").hide()
		player.rotation = Vector3(0, 0, 0)

		get_viewport().arvr = true
		get_viewport().hdr = false
		# keep linear color space, not needed with the GLES2 renderer
		get_viewport().keep_3d_linear = true
		# make sure vsync is disabled or we'll be limited to 60fps
		OS.vsync_enabled = false
		# up our physics to 90fps to get in sync with our rendering
		Engine.iterations_per_second = 90

		player.hide_model()

func stop_arvr():
	if arvr_interface and arvr_interface.interface_is_initialized:
		if OS.has_touchscreen_ui_hint():
			has_touch = true
			HUD.enable_touch_control()
		HUD.crosshair.show()
		HUD.debug_container.show()
		HUD.get_node("MarginContainer").show()

		get_viewport().arvr = false
		get_viewport().hdr = true
		arvr_interface.uninitialize()
		Engine.iterations_per_second = 60

		player.show_model()

func toggle_arvr():
	if get_viewport().arvr:
		stop_arvr()
	else:
		start_arvr()

func init_mobile():
	#OS.show_virtual_keyboard()
	pass

#func init_touchscreen():
#	HUD.enable_touch_control()

func init_web():
	pass

func init_desktop():
	Input.set_mouse_mode(mouse_mode)

func toggle_mouse_mode() -> bool:
	var result : bool
	if (mouse_mode == Input.MOUSE_MODE_CAPTURED):
		mouse_mode = Input.MOUSE_MODE_VISIBLE
		HUD.crosshair.hide()
		HUD.show_top_menu()
		#HUD.left_touch_joystick.hide()
		#HUD.right_touch_joystick.hide()
		result = false
	else:
		mouse_mode = Input.MOUSE_MODE_CAPTURED
		HUD.crosshair.show()
		HUD.hide_top_menu()
		#HUD.left_touch_joystick.show()
		#HUD.right_touch_joystick.show()
		result = true
	Input.set_mouse_mode(mouse_mode)
	return result

#func _exit_tree():
#	print(":: global.gd: _exit_tree()")
