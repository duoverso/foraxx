class_name Player
extends KinematicBody

var mouse_captured = true

const CAN_ROTATE = 1
const CAN_WALK = 2
const CAN_RUN = 4
const CAN_JUMP = 8
const CAN_CROUCH = 16
const CAN_FLY = 32
const CAN_USE_PRIMARY = 64
const CAN_USE_SECONDARY = 128
#const CAN_SWIM
#const CAN_GRAB

export(int) var abilities = CAN_ROTATE | CAN_WALK | CAN_RUN | CAN_JUMP | CAN_CROUCH | CAN_FLY | CAN_USE_PRIMARY | CAN_USE_SECONDARY
#export(int) var abilities = CAN_ROTATE | CAN_WALK | CAN_RUN # | CAN_JUMP | CAN_CROUCH | CAN_FLY | CAN_USE_PRIMARY | CAN_USE_SECONDARY
#export(int) var abilities = 65535

var velocity = Vector3()
export var speed = 500
export var acceleration = 1 # 1/FPS seconds due to processing in _process()
export var run_multiplier = 2
export var max_slope_angle = deg2rad(10)

var gravity
export var gravity_default = -60
export var gravity_max = -150
#onready var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
#PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY, 9.8)
#PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY_VECTOR, Vector3(0, -1, 0))

export var jump_force = 20
var jumping = false
var crouching = false
export(int, "None", "Keyboard", "Mouse") var fly_mode = 0

onready var collision_ray = $CollisionRay
onready var foot_cast = $FootCast
export var foot_cast_translation = Vector3(0, 0.5, 0)
var snap_normal = Vector3.DOWN setget set_snap_normal

var cam
onready var cam_1st = $Camera1st
onready var cam_3rd = $Camera3rd
onready var cam_arvr = $ARVROrigin/ARVRCamera
export var mouse_sensitivity = 10000
export var x_rotation_speed = 0.01
export var y_rotation_speed = 0.01
export var x_rotation_limit_up = -90
export var x_rotation_limit_down = 90
export var x_rotation_start = 0
export var cam_fov_default = 90
export var cam_fov_min = 1
export var cam_fov_max = 120
export var zoom_speed = 1.1

onready var raycast = $Camera1st/RayCast

onready var anim = $AnimationPlayer
onready var skeleton = $Model/Armature/Skeleton
onready var crouchtween = $CollisionRay/CrouchTween
var headbone
var initial_head_transform

export var Tree = [
	preload("res://assets/plants/trees-0/tree_dark1.scn"),
	preload("res://assets/plants/trees-0/tree_dark2.scn"),
	preload("res://assets/plants/trees-0/tree_dark3.scn"),
	preload("res://assets/plants/trees-0/tree_light.scn"),
]
export var Building = preload("res://assets/buildings/simple_house.tscn")

onready var flashlight = $Camera1st/Flashlight

#onready var left_touch_joystick_button = $"/root/Global/HUD/MarginContainer/VBoxContainer/BottomHBoxContainer/LeftTouch/Joystick/Button"
#onready var right_touch_joystick_button = $"/root/Global/HUD/MarginContainer/VBoxContainer/BottomHBoxContainer/RightTouch/Joystick/Button"
var left_touch_joystick_button
var right_touch_joystick_button

#func _enter_tree():
#	print(":: player.gd: _enter_tree() at path: " + get_path())

func _ready():
	#set_process(false)	# Disable _process()

	gravity = gravity_default
	foot_cast.translation = foot_cast_translation

	headbone = skeleton.find_bone("head")
	#cam_1st.translation = skeleton.get_bone_global_pose(headbone).origin
	#initial_head_transform = skeleton.get_bone_pose(headbone)
	cam = cam_1st
	cam.current = true
	fov_reset()
	x_rotation_limit_up = deg2rad(x_rotation_limit_up)
	x_rotation_limit_down = deg2rad(x_rotation_limit_down)
	x_rotation_start = deg2rad(x_rotation_start)
	#rotation.x = x_rotation_start

	call_deferred("when_ready")

func when_ready():
	# TODO: Mouse isn't captured on touch screen
	mouse_captured = true if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED else false
	left_touch_joystick_button = HUD.left_touch_joystick.get_node("Button")
	right_touch_joystick_button = HUD.right_touch_joystick.get_node("Button")

func set_snap_normal(new_snap_normal):
	print("set_snap_normal")
	snap_normal = new_snap_normal

func _physics_process(delta):
#func _process(delta):

	if Global.debug:
		var sen_acc = Input.get_accelerometer()
		if sen_acc:
			HUD.debug_container.get_node("Accelerometer").text = "Accelerometer: " + str(sen_acc)

			#if (cam_1st.current):
			#	# cam_1st.rotation.x -= event.relative.y * x_rotation_speed
			#	cam_1st.rotation.x = -sen_acc.y * 10 # / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
			#	#cam_1st.rotation.x = clamp(cam_1st.rotation.x, x_rotation_limit_up, x_rotation_limit_down)
			#else:
			#	# cam_3rd.rotation.x -= event.relative.y * x_rotation_speed
			#	cam_3rd.rotation.x = -sen_acc.y * 10 # / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
			#	#cam_3rd.rotation.x = clamp(cam_3rd.rotation.x, x_rotation_limit_up, x_rotation_limit_down)
			## rotation.y -= event.relative.x * y_rotation_speed
			#rotation.y = sen_acc.x * 10 # / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)

		var sen_gyro = Input.get_gyroscope()
		if sen_gyro:
			HUD.debug_container.get_node("Gyroscope").text = "Gyroscope: " + str(sen_gyro)
		var sen_grav = Input.get_gravity()
		if sen_grav:
			HUD.debug_container.get_node("Gravity").text = "Gravity: " + str(sen_grav)
		var sen_mag = Input.get_magnetometer()
		if sen_mag:
			HUD.debug_container.get_node("Magnetometer").text = "Magnetometer: " + str(sen_mag)

	var run
	if (abilities & CAN_RUN) and Input.is_action_pressed("run"):
		run = run_multiplier
	else:
		run = 1

	if Input.is_action_pressed("player_enlarge"):
		self.scale_object_local(Vector3(1.01, 1.01, 1.01))
	elif Input.is_action_pressed("player_shrink"):
		self.scale_object_local(Vector3(0.99, 0.99, 0.99))
	elif Input.is_action_pressed("player_size_reset"):
		self.set_scale(Vector3(1.0, 1.0, 1.0))

	elif Input.is_action_pressed("speed_up"):
		# speed = min(1000000, speed*1.01)
		speed *= 1.05
	elif Input.is_action_pressed("slow_down"):
		speed = max(1, speed/1.05)
	elif Input.is_key_pressed(KEY_1):
		speed = 100
	elif Input.is_key_pressed(KEY_2):
		speed = 500
	elif Input.is_key_pressed(KEY_3):
		speed = 2000
	elif Input.is_key_pressed(KEY_4):
		speed = 10000
	elif Input.is_key_pressed(KEY_5):
		speed = 50000
	elif Input.is_key_pressed(KEY_6):
		speed = 200000
	elif Input.is_key_pressed(KEY_7):
		speed = 1000000
	elif Input.is_key_pressed(KEY_8):
		speed = 5000000
	elif Input.is_key_pressed(KEY_9):
		speed = 10000000
	elif Input.is_key_pressed(KEY_0):
		speed = 50000000
		# @TODO: Cause falling at XZ plane
		#self.look_at(Vector3(0, 0, 0), Vector3.UP)

	if abilities & CAN_ROTATE:
		if Input.is_action_pressed("ui_left"):
			rotation.y += y_rotation_speed * 3
		if Input.is_action_pressed("ui_right"):
			rotation.y -= y_rotation_speed * 3

		if Input.is_action_pressed("ui_down"):
			if (cam_1st.current):
				cam_1st.rotation.x -= x_rotation_speed * 3
			else:
				cam_3rd.rotation.x -= x_rotation_speed * 3
		if Input.is_action_pressed("ui_up"):
			if (cam_1st.current):
				cam_1st.rotation.x += x_rotation_speed * 3
			else:
				cam_3rd.rotation.x += x_rotation_speed * 3

	#velocity.x = Input.get_action_strength("strafe_right") - Input.get_action_strength("strafe_left")
	#velocity.z = Input.get_action_strength("forward") - Input.get_action_strength("backward")

	if fly_mode:
		var direction = Vector3()

		if Global.has_touch:
			var direction_joystick = -left_touch_joystick_button.get_value()
			direction.x = direction_joystick.x
			direction.z = direction_joystick.y

		# Fly with keyboard
		if fly_mode == 1:
			if Input.is_action_pressed("forward"):
				direction.z += 1
			if Input.is_action_pressed("backward"):
				direction.z -= 1
			if Input.is_action_pressed("strafe_left"):
				direction.x += 1
			if Input.is_action_pressed("strafe_right"):
				direction.x -= 1
			if Input.is_action_pressed("jump") or Input.is_action_pressed("ui_page_up"):
				direction.y += 1
			if Input.is_action_pressed("crouch") or Input.is_action_pressed("ui_page_down"):
				direction.y -= 1

			direction = direction.normalized().rotated(Vector3.UP, rotation.y)

		elif fly_mode == 2:

			direction.x = sin(rotation.y)
			direction.z = cos(rotation.y)

			if (cam_1st.current):
				#velocity.y = cam_1st.rotation.x * speed * delta # * 100 * delta
				direction.y = cam_1st.rotation.x
				#direction.x = rotation.x # cam_1st.rotation.y
				#direction.y = cam_1st.rotation.z
			else:
				#velocity.y = cam_3rd.rotation.x * speed * delta # * delta
				direction.y = cam_3rd.rotation.x
				#direction.x = rotation.x # cam_3rd.rotation.y
				#direction.y = cam_3rd.rotation.z

			direction = direction.normalized() #.rotated(Vector3.UP, rotation.y)

		#velocity.x = lerp(velocity.x, direction.x, speed * delta)
		#velocity.z = lerp(velocity.z, direction.y, speed * delta)
		velocity.x = direction.x * speed*run * delta
		velocity.y = direction.y * speed*run * delta
		velocity.z = direction.z * speed*run * delta

		#translate(direction)
		# warning-ignore:return_value_discarded
		move_and_slide(velocity, Vector3.UP) #Vector3(0, 1, 0))
		#velocity = move_and_slide(velocity, Vector3.UP)

		#if is_on_floor() and velocity.y < 0:
		if velocity.y < 0 and foot_cast.is_colliding():
			velocity.y = 0
			if jumping:
				jumping = false

	# Falling
	else:
		var direction
		if Global.has_touch:
			direction = -left_touch_joystick_button.get_value()
		else:
			direction = Vector2()
		#print(direction)

		if abilities & CAN_WALK:
			if Input.is_action_pressed("forward"):
				direction.y += 1
			if Input.is_action_pressed("backward"):
				direction.y -= 1
			if Input.is_action_pressed("strafe_left"):
				direction.x += 1
			if Input.is_action_pressed("strafe_right"):
				direction.x -= 1

		if not (jumping or crouching):
			set_anim(direction)

		# Gravity
		velocity.y += gravity * delta
		if velocity.y < gravity_max:
			velocity.y = gravity_max
		# Jump
		#if Input.is_action_just_pressed("jump") and is_on_floor():
		if (abilities & CAN_JUMP) and Input.is_action_just_pressed("jump") and foot_cast.is_colliding():
			velocity.y = jump_force * max(1, speed*run / 500)
			jumping = true
			anim.play("idle")
			anim.play("jump")

		# Crouch
		if (abilities & CAN_CROUCH) and Input.is_action_pressed("crouch"):
			if not (crouching or jumping):
				crouching = true
				anim.play("crouch", 0.2, 1.0)
				crouchtween.interpolate_property(collision_ray.shape, "length", collision_ray.shape.length, 0.5, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				crouchtween.interpolate_property(foot_cast, "translation", foot_cast.translation, foot_cast_translation+Vector3(0, 0.5, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				crouchtween.start()
		# End of crouching
		elif crouching:
			crouching = false
			crouchtween.interpolate_property(collision_ray.shape, "length", collision_ray.shape.length, 1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			crouchtween.interpolate_property(foot_cast, "translation", foot_cast.translation, foot_cast_translation, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
			crouchtween.start()

		#if direction.length() > 0:
		#	direction = direction.normalized() * speed * delta
		if get_viewport().arvr:
			direction = direction.normalized().rotated(-$ARVROrigin/ARVRCamera.rotation.y) # for Vector2 direction
		else:
			direction = direction.normalized().rotated(-rotation.y) # for Vector2 direction

		#velocity.x = lerp(velocity.x, direction.x, speed * delta)
		#velocity.z = lerp(velocity.z, direction.y, speed * delta)
		velocity.x = direction.x * speed*run * delta
		velocity.z = direction.y * speed*run * delta

		#translate(direction)
		#velocity = move_and_slide(velocity, Vector3.UP)
		velocity = move_and_slide(velocity, Vector3.UP, true, 4, max_slope_angle) # Vector3(0, 1, 0)
		#move_and_slide_with_snap(velocity, snap_normal, Vector3.UP, true)

		#if is_on_floor() and velocity.y < 0:
		if velocity.y < 0 and foot_cast.is_colliding():
			velocity.y = 0
			if jumping:
				jumping = false

	#skeleton.set_bone_pose(headbone, initial_head_transform.rotated(Vector3.LEFT, cam_1st.rotation.x))

#	if Input.is_action_pressed("secondary"):
#	#if event.button_index == BUTTON_RIGHT:
#		var tree = Tree.instance()
#		tree.translate(translation) # + Vector3(5, 0, 0))
#		#tree.rotate_object_local(Vector3.UP, rand_range(0, 6.28))
#		#tree.rotate_object_local(Vector3.LEFT, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#		tree.rotate_x(rand_range(-0.1, -0.1))
#		#tree.rotate_object_local(Vector3.FORWARD, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#		tree.rotate_z(rand_range(-0.1, -0.1))
#		#tree.scale_object_local(Vector3(rand_range(9, 11)/10, rand_range(75, 150)/10, rand_range(9,11)/10))
#		tree.rotate_y(rand_range(0, 6.28))
#		get_parent().add_child(tree)

func set_anim(direction):
	if direction == Vector2.ZERO and anim.current_animation != "idle":
		#print("idle")
		anim.play("idle", 0.5)
	elif direction == Vector2(0, 1) and anim.current_animation != "forward" and anim.get_playing_speed() > 0:
		#print("forward")
		anim.play("forward", 0.1)
	elif direction == Vector2(1, 1) and anim.current_animation != "forwardLeft" and anim.get_playing_speed() > 0:
		#print("forwardLeft")
		anim.play("forwardLeft", 0.1)
	elif direction == Vector2(-1, 1) and anim.current_animation != "forwardRight" and anim.get_playing_speed() > 0:
		#print("forwardRight")
		anim.play("forwardRight", 0.1)
	elif direction == Vector2(1, 0) and anim.current_animation != "left":
		#print("left")
		anim.play("left", 0.1)
	elif direction == Vector2(-1, 0) and anim.current_animation != "right":
		#print("right")
		anim.play("right", 0.1)
	elif direction == Vector2(0, -1) and anim.current_animation != "forward": # and anim.get_playing_speed() < 0:
		#print("backward")
		anim.play_backwards("forward", 0.1)
	elif direction == Vector2(-1, -1) and anim.current_animation != "forwardLeft" and anim.get_playing_speed() < 0:
		#print("backwardLeft")
		anim.play_backwards("forwardLeft", 0.1)
	elif direction == Vector2(1, -1) and anim.current_animation != "forwardRight" and anim.get_playing_speed() < 0:
		#print("backwardRight")
		anim.play_backwards("forwardRight", 0.1)

#func process_input_event_key(event):
#	if Input.is_key_pressed(KEY_ESCAPE):
#		get_tree().quit()
#	#var velocity = Vector3()
#	if Input.is_action_pressed("forward"):
#		velocity.z -= 1
#	if Input.is_action_pressed("backward"):
#		velocity.z += 1
#	if Input.is_action_pressed("strafe_left"):
#		velocity.x -= 1
#	if Input.is_action_pressed("strafe_right"):
#		velocity.x += 1
#	if Input.is_action_pressed("ui_page_up"):
#		speed = min(100, speed+1)
#	if Input.is_action_pressed("ui_page_down"):
#		speed = max(1, speed-1)

func rotate_cam(event):
	if not abilities & CAN_ROTATE:
		return

	var relative = event.relative

	#if cam_arvr.current:
	#	cam_arvr.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
	#	cam_arvr.rotation.x = clamp(cam_arvr.rotation.x, x_rotation_limit_up, x_rotation_limit_down)
	if cam_1st.current:
		# cam_1st.rotation.x -= relative.y * x_rotation_speed
		cam_1st.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
		cam_1st.rotation.x = clamp(cam_1st.rotation.x, x_rotation_limit_up, x_rotation_limit_down)
	else:
		# cam_3rd.rotation.x -= relative.y * x_rotation_speed
		cam_3rd.rotation.x -= relative.y / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)
		cam_3rd.rotation.x = clamp(cam_3rd.rotation.x, x_rotation_limit_up, x_rotation_limit_down)

	# rotation.y -= relative.x * y_rotation_speed
	rotation.y -= relative.x / cam.fov / (mouse_sensitivity / cam.fov / cam.fov)

func _input(event):
	#if event is InputEventKey:
	#	process_input_event_key(event)

	if event is InputEventMouseMotion:
		#print("Mouse event")
		# get_global_mouse_position()
		#if (Global.mouse_mode == 2): # Input.MOUSE_MODE_CAPTURED):
		if mouse_captured:
			rotate_cam(event)
	elif event is InputEventScreenDrag:
		#print("Drag event index: ", event.index)
		if event.index == left_touch_joystick_button.ongoing_drag:
			return
		rotate_cam(event)

	elif event.is_pressed():
		if Global.debug:
			HUD.debug_container.get_node("Event").text = event.as_text() + " (Dev" + str(event.get_device()) + ")"
		# Input.is_action_just_released()

		#if Input.is_key_pressed(KEY_ESCAPE):
		if event.is_action("ui_cancel"):
			## Quit the game
			#Settings.save()
			#get_tree().quit()
			mouse_captured = Global.toggle_mouse_mode()

		elif event.is_action("primary"):
			if not mouse_captured or not (abilities & CAN_USE_PRIMARY):
				return
		#if event.button_index == BUTTON_LEFT:
			#print(translation)
#			# Add triangle composed by 3 vertices
#			var vertices = PoolVector3Array()
#			vertices.push_back(Vector3(translation.x, translation.y+1, translation.z))
#			vertices.push_back(Vector3(translation.x+1, translation.y, translation.z))
#			vertices.push_back(Vector3(translation.x, translation.y, translation.z+1))
#			# Initialize the ArrayMesh.
#			var arr_mesh = ArrayMesh.new()
#			var arrays = []
#			arrays.resize(ArrayMesh.ARRAY_MAX)
#			arrays[ArrayMesh.ARRAY_VERTEX] = vertices
#			# Create the Mesh.
#			arr_mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, arrays)
#			var m = MeshInstance.new()
#			m.mesh = arr_mesh
#			get_tree().get_root().add_child(m)
			if raycast.is_colliding():
				var position = raycast.get_collision_point()
				#var normal = raycast.get_collision_normal()
				var tree = Tree[randi() % Tree.size()].instance()
				tree.translate(position) # + Vector3(5, 0, 0))
				#tree.rotate(Vector3.UP, rotation.y)
				tree.rotate(Vector3.UP, deg2rad(randi() % 360))
				tree.scale_object_local(Vector3.ONE * (50+(randi() % 100)) / 100)
				#get_parent().add_child(tree)
				$"/root/Level".add_child(tree)

		elif event.is_action("secondary"):
			if not mouse_captured or not (abilities & CAN_USE_SECONDARY):
				return

			if raycast.is_colliding():
				var position = raycast.get_collision_point()
				var building = Building.instance()
				building.translate(position)
				building.rotate(Vector3.UP, rotation.y + deg2rad(90))
				#get_parent().add_child(building)
				$"/root/Level".add_child(building)

		elif event.is_action("flashlight"):
			flashlight.visible = not flashlight.visible

#		#if event.button_index == BUTTON_RIGHT:
#			var tree = Tree.instance()
#			tree.translate(translation) # + Vector3(5, 0, 0))
#			#tree.rotate_object_local(Vector3.UP, rand_range(0, 6.28))
#			#tree.rotate_object_local(Vector3.LEFT, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#			tree.rotate_x(rand_range(-0.25, -0.25))
#			#tree.rotate_object_local(Vector3.FORWARD, rand_range(-0.25, 0.25)) # rand_range(-0.5, 0.5) like after storm
#			tree.rotate_z(rand_range(-0.25, -0.25))
#			#tree.scale_object_local(Vector3(rand_range(9, 11)/10, rand_range(75, 150)/10, rand_range(9,11)/10))
#			tree.rotate_y(rand_range(0, 6.28))
#			get_parent().add_child(tree)

		#elif event.button_index == BUTTON_WHEEL_UP:
		elif event.is_action("camera_zoom_in"):
			if not mouse_captured:
				return
			if cam.projection == Camera.PROJECTION_PERSPECTIVE:
				cam.fov = max(cam_fov_min, cam.fov / zoom_speed)
			else:
				cam.size /= 1.1

		#elif event.button_index == BUTTON_WHEEL_DOWN:
		elif event.is_action("camera_zoom_out"):
			if not mouse_captured:
				return
			if cam.projection == Camera.PROJECTION_PERSPECTIVE:
				cam.fov = min(cam_fov_max, cam.fov * zoom_speed)
			else:
				cam.size *= 1.1

		elif event.is_action("camera_zoom_reset"):
			fov_reset()

		elif event.is_action("switch_speed"):
			if speed >= 5000:
				speed = 50
			elif speed >= 2000:
				speed = 5000
			elif speed >= 1000:
				speed = 2000
			elif speed >= 500:
				speed = 1000
			elif speed >= 200:
				speed = 500
			elif speed >= 100:
				speed = 200
			elif speed >= 50:
				speed = 100

		#elif event.button_index == BUTTON_MIDDLE:
		elif event.is_action("camera_toggle_view"):
			cam_toggle_view()

		elif event.is_action("camera_bottom_view"):
			cam_bottom_view()
		elif event.is_action("camera_top_view"):
			cam_top_view()
		elif event.is_action("camera_back_view"):
			cam_back_view()
		elif event.is_action("camera_front_view"):
			cam_front_view()
		elif event.is_action("camera_left_view"):
			cam_left_view()
		elif event.is_action("camera_right_view"):
			cam_right_view()

		elif event.is_action("camera_arvr"):
			Global.toggle_arvr()
			cam_arvr.far = cam.far
			cam.current = false
			cam = cam_arvr
			cam.current = true

		elif event.is_action("camera_toggle_projection"):
			cam_toggle_projection()

		elif event.is_action("camera_far_further"):
			cam.far = min(2000000, cam.far * 1.01)
			var environment = LevelEnvironment.env.environment
			environment.fog_depth_end = cam.far
			environment.fog_depth_begin = environment.fog_depth_end / 2
		elif event.is_action("camera_far_closer"):
			cam.far = max(10, cam.far / 1.01)
			var environment = LevelEnvironment.env.environment
			environment.fog_depth_end = cam.far
			environment.fog_depth_begin = environment.fog_depth_end / 2

		# BUTTON_WHEEL_LEFT
		# BUTTON_WHEEL_RIGHT

#		elif event.is_action("crouch"):
#			#if not crouching:
#			#crouching = true
#			anim.play("crouch", 0.2)
#			crouchtween.interpolate_property($CollisionRay.shape, "length", $CollisionRay.shape.length, 0.1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
#			crouchtween.start()

		elif event.is_action("fly_mouse"):
			if not (abilities & CAN_FLY):
				return

			# Turn Mouse Fly Mode off
			if fly_mode == 2:
				#print("falling")
				fly_mode = 0
				gravity = -60
			# Switch to Mouse Fly Mode
			else:
				#print("fly with mouse")
				fly_mode = 2
				gravity = 0
				#velocity.y = 0

		elif event.is_action("fly_keyboard"):
			if not (abilities & CAN_FLY):
				return

			# Turn Keyboard Fly Mode off
			if fly_mode == 1:
				#print("falling")
				fly_mode = 0
				gravity = -60
			# Switch to Keyboard Fly Mode
			else:
				#print("fly with keyboard")
				fly_mode = 1
				gravity = 0
				velocity.y = 0

		elif event.is_action("mouse_mode"):
			mouse_captured = Global.toggle_mouse_mode()

		elif event.is_action("load_game"):
			Settings.load()

		elif event.is_action("save_game"):
			Settings.save()

		##### Events for development #####

		elif event.is_action("return"):
			#translation = Vector3(0, 100, 0)
			LevelManager.player_return_to_start()

		elif event.is_action("go_to_lobby"):
			#print("go to lobby\n")
			#get_tree().change_scene("res://scenes/Level-0.tscn")
			LevelManager.goto_level("constructor")

		elif event.is_action("next_level"):
			LevelManager.next_level()

		elif event.is_action("previous_level"):
			LevelManager.previous_level()

		elif event.is_action("random_environment"):
			LevelEnvironment.rand_environment()

		elif event.is_action("pause"):
			get_tree().paused = true
			# Global.set_pause_mode(Node.PAUSE_MODE_STOP)

	# released = not is_pressed()
	#else:
#		if event.is_action("crouch"):
#			#if crouching:
#			#crouching = false
#			crouchtween.interpolate_property($CollisionRay.shape, "length", $CollisionRay.shape.length, 1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN)
#			crouchtween.start()

		#zoom = clamp(zoom, zoom_min, zoom_max)

		#print_debug(event)
		#print(event)
	#print(event)

func show_model():
	if not get_viewport().arvr:
		$Model.show()

func hide_model():
	if get_viewport().arvr or cam != cam_3rd:
		$Model.hide()

# warning-ignore:function_conflicts_variable
func cam_1st() -> void:
	cam_1st.far = cam.far
	cam_1st.projection = cam.projection
	cam_1st.fov = cam.fov
	cam_1st.size = cam.size
	cam.current = false
	cam = cam_1st
	cam.current = true
	HUD.crosshair.show()
	hide_model()

# warning-ignore:function_conflicts_variable
func cam_3rd() -> void:
	cam_3rd.far = cam.far
	cam_3rd.projection = cam.projection
	cam_3rd.fov = cam.fov
	cam_3rd.size = cam.size
	cam.current = false
	cam = cam_3rd
	cam.current = true
	HUD.crosshair.hide()
	show_model()

func cam_toggle_view() -> void:
	if cam_1st.current:
		cam_3rd()
	else:
		cam_1st()

func cam_bottom_view() -> void:
	rotation.y = 0
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(90)

func cam_top_view() -> void:
	rotation.y = 0
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(-90)

func cam_back_view() -> void:
	rotation.y = deg2rad(180)
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_front_view() -> void:
	rotation.y = 0
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_left_view() -> void:
	rotation.y = deg2rad(-90)
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_right_view() -> void:
	rotation.y = deg2rad(90)
	#cam_1st()
	cam_orthogonal()
	cam.rotation.x = deg2rad(0)

func cam_perspective() -> void:
	cam.projection = Camera.PROJECTION_PERSPECTIVE
	# @TODO There should be some conversion ratio
	cam.fov = cam.size

func cam_orthogonal() -> void:
	cam.projection = Camera.PROJECTION_ORTHOGONAL
	# @TODO There should be some conversion ratio
	cam.size = cam.fov

func cam_toggle_projection() -> void:
	# Projection: PROJECTION_PERSPECTIVE=0, PROJECTION_ORTHOGONAL=1, PROJECTION_FRUSTUM=2
	if cam.projection == Camera.PROJECTION_PERSPECTIVE:
		cam_orthogonal()
	else:
		cam_perspective()

func fov_reset() -> void:
	cam.fov = cam_fov_default

#func _physics_process(delta):
#	pass

#func _exit_tree():
#	print(":: player.gd: _exit_tree()")
