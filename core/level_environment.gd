extends Node

var environments = [
	#"default_env",
	#"fs003_day_sunless",
#	"fs003_day",
	#"fs003_night_moonless",
#	"fs003_night",
#	"fs003_rainy",
#	"fs003_snowy",
#	"fs003_sunrise", # dark orange ground with sun & clouds
#	"fs003_sunset", # orange ground with sun & clouds
#	"starmap",		# space night with real stars seen from Earth
#	"tutsplus_001", # (sunrise blue) ocean with clouds
#	"tutsplus_002", # (sunrise orange) ocean with clouds
#	"tutsplus_003", # (dark) ocean with clouds
#	"tutsplus_004", # (half-light) ocean with big cloud at half of the sky
#	"tutsplus_005", # (pink) ocean at sunset
#	"tutsplus_006", # ocean at sunset with stars & moon
#	"tutsplus_007", # ocean at sunset with clouds & moon
#	"tutsplus_008", # (blue) ocean at night with stars, planet & moon
]

#export var environments_dir = "res://assets/environment"
export var environments_dir_user = "user://environment"

var env
# See https://github.com/godotengine/godot-demo-projects/blob/3.0-d69cc10/misc/threads/thread.gd
var thread = Thread.new()

func _ready():
	#env = get_tree().get_root().get_node("Global/WorldEnvironment")
	env = get_node("/root/Global/WorldEnvironment")
	load_environments()
	#get_tree().quit()

func load_environments() -> void:
	environments = []
	var dir = Directory.new()
	if !dir.dir_exists(environments_dir_user):
		dir.make_dir(environments_dir_user)
	if dir.open(environments_dir_user) != OK:
		printerr("!! An error occurred when trying to look up for environments at " + environments_dir_user)
	else:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			#if dir.current_is_dir():
				# @todo Recursive look up for levels
				#pass
			# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
			if not dir.current_is_dir() and file_name.match("*.tres"):
				#print("Level found: " + file_name)
				environments.append(file_name.get_basename())
			file_name = dir.get_next()

	if environments.size() == 0:
		printerr("!! No environment found")

	print("## Environments: ", environments)

#func env():
	#return current_scene.get_node("WorldEnvironment")
	#return $"/root/Level/WorldEnvironment"
	#return $"/root/Global/WorldEnvironment"

func set_environment(new_env) -> void:
	if (thread.is_active()):
		return

	if new_env in environments:
		print("@@ Environemnt: ", new_env)
		thread.start(self, "set_environment_threaded", new_env, Thread.PRIORITY_LOW)
		#thread.call_deferred("wait_to_finish")
	else:
		printerr("!! Unknown environment: ", new_env)

func set_environment_threaded(new_env):
	var fog_enabled = env.environment.fog_enabled
	var fog_depth_begin = env.environment.fog_depth_begin
	var fog_depth_end = env.environment.fog_depth_end
	env.environment = load(environments_dir_user + "/" + new_env + ".tres")
	env.environment.fog_enabled = fog_enabled
	env.environment.fog_depth_begin = fog_depth_begin
	env.environment.fog_depth_end = fog_depth_end
	env.environment.fog_color = Color.black
	thread.call_deferred("wait_to_finish")
#	call_deferred("set_environment_done")
#	#return res

#func set_environment_done():
#	#var res = thread.wait_to_finish()
#	thread.wait_to_finish()

func rand_environment() -> void:
	var rand_env = environments[randi() % environments.size()]
	print("@@ Random environment: " + rand_env)
	set_environment(rand_env)
