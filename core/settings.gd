extends Node

var _loaded = false
var _settings_file = "user://settings.json"
var _config_file = "user://settings.cfg"

var config = ConfigFile.new()

func _enter_tree():
	if Settings._loaded:
		printerr("Error: Settings is an AutoLoad singleton and it shouldn't be instanced elsewhere.")
		printerr("Please delete the instance at: " + get_path())
	else:
		Settings._loaded = true

func save():
	save_config()
	#save_settings()

func load():
	load_config()
	#load_settings()

func save_settings():
	var file = File.new()
	file.open(_settings_file, File.WRITE)
	var data = {
		#"render_distance": render_distance,
		#"fog_enabled": fog_enabled,
	}
	file.store_line(to_json(data))
	file.close()

func load_settings():
	var file = File.new()
	if file.file_exists(_settings_file):
		file.open(_settings_file, File.READ)
		while file.get_position() < file.get_len():
			# Get the saved dictionary from the next line in the save file
			var data = parse_json(file.get_line())
			# warning-ignore:unused_variable
			var render_distance = data["render_distance"]
			#fog_enabled = data["fog_enabled"]
		file.close()
	#else:
		#save_settings()

func save_config():
	print("<> Save config to: " + _config_file)
	#config.set_value("level", "number", Global.current_scene)
	#print(LevelManager.current_scene)
	var player = Global.player
	var cam_1st = player.cam_1st
	var cam_3rd = player.cam_3rd
	config.set_value("player", "translation", player.translation)
	config.set_value("player", "rotation", player.rotation)
	config.set_value("player", "speed", player.speed)
	config.set_value("player", "gravity", player.gravity)
	config.set_value("player", "fly_mode", player.fly_mode)
	if cam_1st == player.cam:
		config.set_value("player", "cam", "1st")
	else:
		config.set_value("player", "cam", "3rd")
	config.set_value("player", "cam_far", player.cam.far)
	config.set_value("player", "cam_1st_fov", cam_1st.fov)
	config.set_value("player", "cam_1st_rotation", cam_1st.rotation)
	config.set_value("player", "cam_3rd_fov", cam_3rd.fov)
	config.set_value("player", "cam_3rd_rotation", cam_3rd.rotation)
	# Store a variable if and only if it hasn't been defined yet
	#if not config.has_section_key("audio", "mute"):
	#	config.set_value("audio", "mute", false)
	config.save(_config_file)
	HUD.set_event_text("Game saved", 1.0)

func load_config():
	print("<> Load config from: " + _config_file)
	# ConfigFile (see https://docs.godotengine.org/en/latest/classes/class_configfile.html)
	var err = config.load(_config_file)
	if err == OK: # If not, something went wrong with the file loading
		# Look for the display/width pair, and default to 1024 if missing
		#screen_width = config.get_value("display", "width", 1024)
		var player = Global.player
		var cam_1st = player.cam_1st
		var cam_3rd = player.cam_3rd
		player.translation = config.get_value("player", "translation", player.translation)
		player.rotation = config.get_value("player", "rotation", player.rotation)
		player.speed = config.get_value("player", "speed", player.speed)
		player.gravity = config.get_value("player", "gravity", player.gravity)
		player.fly_mode = config.get_value("player", "fly_mode", player.fly_mode)
		if config.get_value("player", "cam", "1st") == "1st":
			player.cam = cam_1st
		else:
			player.cam = cam_3rd
		player.cam.far = config.get_value("player", "cam_far", player.cam.far)
		player.cam.current = true
		cam_1st.fov = config.get_value("player", "cam_1st_fov", cam_1st.fov)
		cam_1st.rotation = config.get_value("player", "cam_1st_rotation", cam_1st.rotation)
		cam_3rd.fov = config.get_value("player", "cam_3rd_fov", cam_3rd.fov)
		cam_3rd.rotation = config.get_value("player", "cam_3rd_rotation", cam_3rd.rotation)
		HUD.set_event_text("Game loaded", 1.0)

#func _exit_tree():
#	print(":: settings.gd: _exit_tree()")
