# DownloadManager
#
# Try: DownloadManager.download("https://via.placeholder.com/512", "user://placeholder-512.png")
#
extends Node

var assets_url = "https://foraxx.duoverso.com/assets"

# User files multiple-level Dictionary to map assets to local filesystem
# Array value is for list of files
# "_files" key of Dictionary is for list of files too but allows to map subdirectories too
var user_files = {
	"environment": {
		"_files": [
			"fs003_day.tres",
			"fs003_night.tres",
			"fs003_rainy.tres",
			"fs003_snowy.tres",
			"fs003_sunrise.tres",
			"fs003_sunset.tres",
			"starmap.tres",
			"tutsplus_001.tres",
			"tutsplus_002.tres",
			"tutsplus_003.tres",
			"tutsplus_004.tres",
			"tutsplus_005.tres",
			"tutsplus_006.tres",
			"tutsplus_007.tres",
			"tutsplus_008.tres",
		],
		"sky": [
			"FS003_Day.png.import",
			"FS003_Day.png.md5",
			"FS003_Day.png.stex",
			"FS003_Day_Sunless.png.md5",
			"FS003_Night_Moonless.png.md5",
			"FS003_Night.png.import",
			"FS003_Night.png.md5",
			"FS003_Night.png.stex",
			"FS003_Rainy.png.import",
			"FS003_Rainy.png.md5",
			"FS003_Rainy.png.stex",
			"FS003_Snowy.png.import",
			"FS003_Snowy.png.md5",
			"FS003_Snowy.png.stex",
			"FS003_Sunrise.png.import",
			"FS003_Sunrise.png.md5",
			"FS003_Sunrise.png.stex",
			"FS003_Sunset.png.import",
			"FS003_Sunset.png.md5",
			"FS003_Sunset.png.stex",
			"starmap.tres",
			"starmap_4k.jpg.import",
			"starmap_4k.jpg.md5",
			"starmap_4k.jpg.stex",
			"tutsplus_001.hdr.import",
			"tutsplus_001.hdr.md5",
			"tutsplus_001.hdr.stex",
			"tutsplus_002.hdr.import",
			"tutsplus_002.hdr.md5",
			"tutsplus_002.hdr.stex",
			"tutsplus_003.hdr.import",
			"tutsplus_003.hdr.md5",
			"tutsplus_003.hdr.stex",
			"tutsplus_004.hdr.import",
			"tutsplus_004.hdr.md5",
			"tutsplus_004.hdr.stex",
			"tutsplus_005.hdr.import",
			"tutsplus_005.hdr.md5",
			"tutsplus_005.hdr.stex",
			"tutsplus_006.hdr.import",
			"tutsplus_006.hdr.md5",
			"tutsplus_006.hdr.stex",
			"tutsplus_007.hdr.import",
			"tutsplus_007.hdr.md5",
			"tutsplus_007.hdr.stex",
			"tutsplus_008.hdr.import",
			"tutsplus_008.hdr.md5",
			"tutsplus_008.hdr.stex",
		]
	},
	"music": [
		"bamboo-forest.ogg.import",
		"bamboo-forest.ogg.md5",
		"bamboo-forest.ogg.oggstr",
		"forest-ambience.ogg.md5",
		"forest-ambience.ogg.oggstr",
		"forest-ambience.ogg.import",
		"forest.ogg.md5",
		"forest.ogg.oggstr",
		"forest.ogg.import",
		"for-the-king.ogg.md5",
		"for-the-king.ogg.oggstr",
		"for-the-king.ogg.import",
		"he-will-never-see-her-again.ogg.import",
		"he-will-never-see-her-again.ogg.md5",
		"he-will-never-see-her-again.ogg.oggstr",
		"magical-forest.ogg.md5",
		"magical-forest.ogg.oggstr",
		"magical-forest.ogg.import",
	]
}

var is_downloaded = false

func download(url : String, file_name : String):
	var http = HTTPRequest.new()
	add_child(http)
	#http.connect("request_completed", self, "_http_request_completed")
	http.set_use_threads(true)
	http.set_download_file(file_name)
	var error = http.request(url)
	#print("<> Downloaded " + url + " (" + str(error) + ") to ", file_name)
	print("<> Download ", file_name, " (" + str(error) + ")")

#func _http_request_completed(result, response_code, headers, body):
#	var response = parse_json(body.get_string_from_utf8())
#	print(response.headers["User-Agent"])


func check_files(path : String, dict : Dictionary):
	var result = []
	var file_check = File.new()
	for key in dict:
		var value = dict[key]
		# Traverse Dictionaries recursively
		if value is Dictionary:
			result += check_files(path + "/" + key, value)
		# Traverse Array as list of filenames
		elif value is Array:
			for file in value:
				var file_path
				# "_files" key of Dictionary isn't subdirectory
				if key == "_files":
					file_path = path + "/" + str(file)
				else:
					file_path = path + "/" + str(key) + "/" + str(file)
				if (!file_check.file_exists(file_path)):
					result.append(file_path)
		# Check String as filename
		elif value is String:
			var file_path = path + "/" + value
			if (!file_check.file_exists(file_path)):
				result.append(file_path)
		else:
			printerr("!! Bad type of user_file (path: ", path, "/", key, "): ", value)
	return result

func download_user_files():
	return
	var to_download = check_files("user:/", user_files) # second slash will be appeded to filenames as root folder
	for file in to_download:
		var url = assets_url + file.replace("user:/", "")
		download(url, file)
	is_downloaded = true
	user_files = {} # Free memory
