# @TODO Directory.open() doesn't work on Android (https://github.com/godotengine/godot/issues/39474)
extends Node

export var debug := false

var _loaded := false

export var use_stream_player := true
#export var music_dir = "res://assets/music"
export var music_dir_user := "user://music"
#export var music_dirs := ["res://assets/music", "user://music"]
export var midi_use_player := false
#export var midi_dirs := ["res://assets/music/midi", "user://midi"]
export var is_playing := false
export var track_number := 0
export var has_shuffle := true

var tracks := []
var dir := Directory.new()

onready var music_player = $AudioStreamPlayer
var midi_player

func _enter_tree() -> void:
	if _loaded:
		printerr("Error: Music is an AutoLoad singleton and it shouldn't be instanced elsewhere.")
		printerr("Please delete the instance at: " + get_path())
	else:
		_loaded = true

#func _get_property_list():
#	return [
#		{ name = "MIDI", type = TYPE_NIL, hint_string = "midi_", usage = PROPERTY_USAGE_GROUP }
#	]

func _ready() -> void:
	randomize() # for shuffle

	if use_stream_player:
		#connect("finished" , music_player, "play_next")
		load_tracks()

		if is_playing and not tracks.empty():
			if has_shuffle:
				# warning-ignore:return_value_discarded
				play_next()
			else:
				play_track(track_number)

	if midi_use_player:
		midi_player = MidiPlayer.new()
		midi_player.name = "MidiPlayer"
		#midi_player.load_all_voices_from_soundfont = false
		midi_player.set_soundfont("res://assets/audio/soundfonts/Aspirin-Stereo.sf2")
		midi_player.set_volume_db(-20) # 0 = toooo loud, -70 = too quiet
		midi_player.set_tempo(120)
		add_child(midi_player)

		if is_playing:
			midi_player.set_file("res://assets/music/midi/abuse/INTRO.MID")
			#midi_player.playing = true
			midi_player.play()
		#midi_player.stop()

func _input(event) -> void:
	if event.is_pressed():
		if event.is_action("music_toggle_playing"):
			# warning-ignore:return_value_discarded
			toggle_playing()
		elif event.is_action("music_next"):
			# warning-ignore:return_value_discarded
			play_next()
		elif event.is_action("music_toggle_shuffle"):
			has_shuffle = not has_shuffle

func play_next() -> int:
	if has_shuffle:
		track_number = randi() % tracks.size()
	else:
		track_number += 1
		if (track_number >= tracks.size()):
			track_number = 0
	play_track(track_number)
	return track_number

func set_shuffle(new_value : bool) -> void:
	print("&& Music shuffle " + str(new_value))
	has_shuffle = new_value

func toggle_shuffle() -> bool:
	set_shuffle(!has_shuffle)
	return has_shuffle

func stop() -> void:
	print("&& Music stopped")
	music_player.stop()

func toggle_playing() -> bool:
	is_playing = not is_playing
	if is_playing:
		play_track(track_number)
	else:
		stop()
	return is_playing

func load_tracks() -> void:
	tracks = []
	# Music in main package
#	if dir.open(music_dir) != OK:
#		print("An error occurred when trying to look up for music tracks at " + music_dir)
#	else:
#		dir.list_dir_begin()
#		var file_name = dir.get_next()
#		while file_name != "":
#			#if dir.current_is_dir():
#				# @todo Recursive look up for music tracks
#				#pass
#			# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
#			if not dir.current_is_dir() and file_name.match("*.import"):
#				file_name = file_name.replace(".import", "")
#				#tracks.append(music_dir + "/" + file_name)
#				tracks.append(file_name)
#			file_name = dir.get_next()

	# Music at user directory
	if !dir.dir_exists(music_dir_user):
		# warning-ignore:return_value_discarded
		dir.make_dir(music_dir_user)
	if dir.open(music_dir_user) != OK:
		printerr("An error occured when trying to look up for music tracks at " + music_dir_user)
	else:
		# Download music

		# Scan music
		# warning-ignore:return_value_discarded
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if not dir.current_is_dir() and file_name.match("*.import"):
				file_name = file_name.replace(".import", "")
				#tracks.append(music_dir_user + "/" + file_name)
				tracks.append(file_name)
			file_name = dir.get_next()

	if tracks.size() == 0:
		is_playing = false
		printerr("!! No music tracks found")
	elif debug:
		print("## Music tracks: ", tracks)
	else:
		print("## Music tracks found: ", tracks.size())

func play_file(track_url : String) -> bool:
	# Check if file still exists
	#if not dir.file_exists(track_url + ".import"):
	if not ResourceLoader.exists(track_url):
		printerr("Music track ", track_url, " is missing.")
		return false
	var track = load(track_url)
	music_player.stream = track
	music_player.play()
	return true

func play_track(number : int) -> void:
	var track_url = music_dir_user + "/" + tracks[number]
	print("♫♫&& Playing music track: " + track_url)
	is_playing = play_file(track_url)

func _on_AudioStreamPlayer_finished() -> void:
	if is_playing:
		# warning-ignore:return_value_discarded
		play_next()

#func _exit_tree():
#	print(":: music.gd: _exit_tree()")
