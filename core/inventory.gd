extends Node

var items_file = "res://assets/items/all.json"

var items: Dictionary

func _ready():
	items = load_items()

func load_items():
	var file = File.new()
	file.open(items_file, File.READ)
	var json = JSON.parse(file.get_as_text())
	file.close()
	return json.result
