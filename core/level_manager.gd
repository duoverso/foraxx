extends Node

var _loaded = false

export var _levels_dir = "res://assets/levels"

#var levels
var levels = {
	# index have to be same as name of .tscn or .scn file
	"constructor": { "name": "The Constructor", "open": true },

	"desert": { "name": "The Desert", "open": true },
	"hills": { "name": "The Hills" },
	"canyon": { "name": "The Canyon" },
	"canyons": { "name": "The Canyons" },
	"flatstones": { "name": "The Flatstones" },
	"riverland": { "name": "The Riverland" },
	"lakes": { "name": "The Lakes" },
	"dragon_castle": { "name": "The Dragon Castle" },

	"dragons": { "name": "The Land of the Dragons" },

	"infinite": { "name": "The Tiled World", "open": true },
}
# for testing
var _levels_keys

var current_scene
var current_level_key

func _enter_tree():
	#print(":: level_manager.gd: _enter_tree() at path: " + get_path())
	if LevelManager._loaded:
		printerr("Error: LevelManager is an AutoLoad singleton and it shouldn't be instanced elsewhere.")
		printerr("Please delete the instance at: " + get_path())
	else:
		LevelManager._loaded = true

func _ready():
	# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
	#load_levels()

	#print(":: level_manager.gd: _ready()")
	_levels_keys = levels.keys()
	print("## Levels: ", _levels_keys)

	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)
	current_level_key = find_current_level_key()
	#print("@@ Start level: " + current_level_key + " (with " + str(current_scene.get_world()) + ")")
	print("@@ Start level: " + current_level_key)

	#LevelEnvironment.rand_environment()

func find_current_level_key():
	return current_scene.get_filename().get_basename().get_file()

#func get_level_settings(level_key: String):
#	return levels[level_key]

func get_current_level_settings():
	return levels[current_level_key]

func load_levels():
	levels = []
	var dir = Directory.new()
	if dir.open(_levels_dir) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			#if dir.current_is_dir():
				# @todo Recursive look up for levels
				#pass
			# Only .import files are read on Android (see https://github.com/godotengine/godot/issues/39474)
			if not dir.current_is_dir() and file_name.match("*.tscn"):
				#print("Level found: " + file_name)
				levels.append(file_name.get_basename())
			file_name = dir.get_next()
	else:
		printerr("An error occurred when trying to look up for levels at " + _levels_dir)

	if levels.size() == 0:
		printerr("Can't find any level at " + _levels_dir)

	#print("@@ At " + _levels_dir + " found levels: " + str(levels))
	print("## Levels: ", levels)

func goto_level(level_key: String, trans = null):
	goto_scene(_levels_dir + "/" + level_key + ".tscn", trans)

func goto_scene(path: String, location = null):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path, location)

func _deferred_goto_scene(path: String, location = null):
	if current_scene.get_filename() != path:
		# It is now safe to remove the current scene
		current_scene.free()
		# Load the new scene.
		var s = ResourceLoader.load(path)
		# Instance the new scene.
		current_scene = s.instance()
		current_level_key = find_current_level_key()
		# Add it to the active scene, as child of root.
		get_tree().get_root().add_child(current_scene)
		print("@@ Current level: " + current_level_key + " (of " + str(current_scene.get_world()) + ")")
		# Optionally, to make it compatible with the SceneTree.change_scene() API.
		get_tree().set_current_scene(current_scene)

	#LevelEnvironment.rand_environment()

	# Relocate player
	if location is Vector3:
		print("@@ Relocating player to: ", location)
		Global.player.translation = location
	#elif trans == "PlayerStart":
	else:
		player_return_to_start()

	#HUD.set_event_text("Welcome at " + levels[current_level_key].name, 5.0)
	var level_name = current_scene.get("level_name")
	if level_name:
		HUD.set_event_text("Welcome at " + level_name, 5.0)

func player_return_to_start():
	var player = Global.player
	if current_scene.has_node("PlayerStart"):
		var player_start = current_scene.get_node("PlayerStart")
		player.translation = player_start.translation + Vector3(0, 2, 0)
		player.rotation = player_start.rotation
		#player.rotation = Vector3(0, 0, 0)
	else:
		player.translation = Vector3(0, 100, 0)
		player.rotation = Vector3(0, 0, 0)
	HUD.dialogue_big_close()
	HUD.dialogue_close()

# for testing
func next_level():
	var next_level_id = _levels_keys.find(current_level_key) + 1
	next_level_id = wrapi(next_level_id, 0, _levels_keys.size())
	goto_scene(_levels_dir + "/" + _levels_keys[next_level_id] + ".tscn")

# for testing
func previous_level():
	var previous_level_id = _levels_keys.find(current_level_key) - 1
	previous_level_id = wrapi(previous_level_id, 0, _levels_keys.size())
	goto_scene(_levels_dir + "/" + _levels_keys[previous_level_id] + ".tscn")

#func _exit_tree():
#	print(":: level_manager.gd: _exit_tree()")
