# Software

- Godot
- Blender
- GIMP

# Godot tutorials
- [Gonkee](https://www.youtube.com/c/Gonkee/)
- [Procedural terrain](https://www.youtube.com/watch?v=rWeQ30h25Yg)

# 3D models

- assets/
	- animals/
		- bear/ :: https://sketchfab.com/3d-models/bear-27ce91e2f2754ca8bb0b00fe2644ae39
		- cat/ :: blendswap.com/6897-lowpoly-siamese-cat
		- dragon_tutu/
		- griffin/ :: https://sketchfab.com/3d-models/griffin-animated-a8852f113416426bb06e6bba49a525a9
		- seagull/ :: blendswap.com/25659-seagull-low-poly
		- wolf/
		- wolf-anim/ :: https://sketchfab.com/3d-models/wolf-e319f122c2474d3186b841b019fb0edd
	- buildings/
		- scifi-corridor/ :: blendswap.com/18801-sci-fi-corridor
	- characters/
		- battle-mecha/ :: blendswap.com/21962-battle-mecha
	- environment/sky/ :: cubebrush.co/Fantasy Skybox FREE, cgi.tutsplus.com/Cgtuts_OceanHDRIs_Freebie
	- machines/
		- vending_machine :: blendswap.com/25644-vending-machine
	- plants/
		- bamboo/ :: https://opengameart.org/content/stylish-plants
		- bush/ :: https://opengameart.org/content/stylish-plants
		- fern/ ::
		- hemp/ :: https://opengameart.org/content/stylish-plants
		- low-poly-realistic-tree/ :: blendswap.com/7128-low-poly-realistic-tree
		- swirl/ :: https://opengameart.org/content/stylish-plants
		- trees/ :: 
		- trees-0/ :: https://opengameart.org/content/low-poly-trees-stumps-branches
		- white-flower/ :: https://opengameart.org/content/stylish-plants
		- yggdrassil/ :: https://sketchfab.com/3d-models/tree-ea5e6ed7f9d6445ba69589d503e8cebf
		- leaves-shader.material :: https://docs.godotengine.org/en/stable/tutorials/content/making_trees.html
	- terrain/ :: Blender plugin A.N.T.Landscape
		- castle/ :: https://opengameart.org/content/3d-castle-dungeon-tileset-extended
		- golf/ :: https://docs.godotengine.org/en/stable/tutorials/3d/using_gridmaps.html
		- lpn/ :: cubebrush.co/Low-poly Style Nature
	- vehicles/
		- low-poly-car/ :: blendswap.com/25749-low-poly-car
		- low-poly-ducati/ :: blendswap.com/25037-low-poly-ducati

# Animations

## Motion capture

# Music