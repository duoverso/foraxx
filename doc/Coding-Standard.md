# Coding Standard

## Naming Conventions
- MyClass = PascalCase
- some_variable, any_method(), all_files = snake_case
- MY_CONSTANT = ALL_CAPS
- Node_name = # _ are needed for $Node symbol address, - doesn't work