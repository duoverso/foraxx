# How to make terrain

## High-poly in Blender
- Use Blender A.N.T. Landscape addon
- Change Mesh Size X & Mesh Size Y to 1000 [m], and multiply Noise Size, Height & Maximum by 1000 too
- Enable Falloff XY and set Falloff X and Y to best value between 10 and 100
- Save preset with Subdivisions X and Y: 128
- Use preset and change Subdivisions X and Y to 512 to produce terrain (~500k tris)
- {Object mode} Move mesh to positive XY coordinates and change origin of object to 0,0
? {Edit mode} Unwrap > Cube Projection with Cube Size: 0.5 (real texture size is 2×2m)
- Add material with texture
- {Object mode} Duplicate mesh and hide this high-poly as backup

## Low-poly in Blender
- {Edit mode} Smooth Vertices with Smoothing: 1 and Repeat: 2
- Mesh > Clean up > Decimate Geometry with Ratio: 0.05 (~25k tris)
- Select all edges one by one and aply Scale 0 in Z and X or Y axis to make perfect rectangle edge with Z: 0
- {Object mode} Scale to 1000x1000 [m] and Apply Scale
- {Edit mode + Top view} Unwrap > Project from View (Bounds) and scale UV map 500x (real texture size is 2×2m)
- Recalculate Normals
- Export to Godot's .escn (use Spatial Material)

## Import to Godot
- Fix material: Albedo & Normal texture, Roughness, set cull_mode to Disabled
- Rename root Spatial node to "Level"
- Attach GDScript and define variables: level_file, level_name
- Merge WorldEnvironment and DirectLight from another Level
- Rename Landscape mesh and change type to CSGMesh, enable collisions
- Add Spatial node with name "PlayerStart" and place it where player should start
- Save as .tscn and remove .escn