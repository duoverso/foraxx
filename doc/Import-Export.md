Blender Import&Export for Foraxx
================================

!!! Test
Into new godot project:
- copy .jpg, .png, .tga (one at a time) and check how much size is spend by .import files (check .import/ folder too)

@@@ TODO @@@
- do not export assets/environment/tutsplus_* and assets/environment/sky/tutsplus_*
- Remove all non-png images
- export from Godot, import to Blender
	https://godotengine.org/article/introducing-the-godot-gltf-2-0-scene-exporter

# Before Import
- convert images to optimized format for size
	- detect if image is transparent
		0) convert all images to PNG, then try to guess which images could be converted to jpeg (with specified quality)
		1) easy solution by extension - .tga are consider transparent, etc.
		2) by detection of transparency pixels in image
		3) by usage - normal texture could be converted to non-transparent even when source is transparent tga
	- convert non-transparent images to JPEG with 80% quality
	- convert transparent images to PNG

# Import
- Import in Blender 2.79, 2.82, 2.90...
- Import from FBX, OBJ, DAE, XPS, glTF, .blend...
- Import one or multiple files
- Use predefined file for entire folder/subfolders (eg. to generate texture thumnails)

# Get info after import
- number of vertices, polygons, triangles
- list of meshes, materials and associated textures, armature nodes

# Operations in Blender
- Scale, Move, Rotate (change relative value by operator or change absolute values)
- Apply Scale, Location, Rotation
- Move origin
- Delete nodes (meshes, materials, textures, bones of armature)
- Change materials of meshes
	- all materials Use Nodes
	- all materials have to be Principled BSDF (convert especialy XPSShader)
	- change textures
	- change Metallic, Specular, Roughness, Emission, Alpha, NormalMap
- Retarget rig and merge animations from another file
- Save screenshot from Blender editor
- Render thumbnails
- Generate LOD variants

# Export
- Export to .escn, .dae, .fbx...

# After Export
- Convert from .escn to .tscn

# Everything defined in external file
- process everything by data in external file (eg. .json)
	- execute multiple operations (even same) in specified order
- definition files are generated from external tools
	- scripts
	- html5 viewer/debugger
	- drag&drop to online portal with models
		- automaticaly generate thumbnails & convert model(s) to multiple formats