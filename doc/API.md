API
===

# Command Line Arguments
`--level`
`--position`
`--player <model>`
`--nick <name>`
`--login <login> --password <password>`

`--server`

# camera-follow.gd

# global.gd
Look at `core/*.gd` scripts and to *Project / Project Settings...* > *Input Map* tab.
```
Global.debug
Global.mouse_mode
Global.touch
Global.start_in_arvr
Global._environments_dir
Global.env
Global.player

Global.start_arvr()
Global.stop_arvr()
Global.toggle_arvr()
Global.init_mobile()
Global.init_desktop()
Global.init_web()
Global.load_environments()
Global.set_environment()
Global.rand_environment()
```