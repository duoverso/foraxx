Dialogues
=========

# Intro
In year 2029 the developers inspired by The Matrix movie made
open-source AR/VR environment called **Foraxx** as simulation of entire galaxy
with limitless possibilities. The life on Earth then changed as never before.

**Morpheus|BlackBear>** Welcome <PlayerName>,
	it's an honor to show right to you how you can control your virtual body in our training program.

**Trinity|WhiteRabbit>**

**Architect|Turtle>** And i will teach you how to take control of the entire program.

**Oracle|DarkOwl>** I will help you anytime you need.

**Smith|Spider>**