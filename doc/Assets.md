Assets
======

# Map layers
Z
0 ocean
1 seas
2 territories
3 islands, reefs
4 mountains
5 forests
6 lakes
7 rivers
8 cities
9 buildings
10 roads
11 bridges
12 specials
13 unknown

# Materials
- https://www.blendswap.com/blend/25508

## Vehicles

### Animals
- horse
- dragon

### Small (can be taken)
- parachute
- ski
	- intermediate https://www.blendswap.com/blend/12381
	- begginer https://www.blendswap.com/blend/13118
	- advanced https://www.blendswap.com/blend/12349
	- racing https://www.blendswap.com/blend/12945
- snowshoes

### Large (player is placed inside)
- chariot https://www.blendswap.com/blend/13170
- car
- train https://www.blendswap.com/blend/25461
- balloon
- airship https://www.blendswap.com/blend/25430
- hovercraft
- airplane
	- with parachute (https://www.blendswap.com/blend/25522)
	- with floats https://www.blendswap.com/blend/25363
- spaceship
- submarine (https://www.blendswap.com/blend/25587)

# Weapons
- sets
	- (670kb) https://www.blendswap.com/blend/22839
	- https://www.blendswap.com/blend/24379
- sword
	- https://www.blendswap.com/blend/24514

- dagger https://www.blendswap.com/blend/25339
- shield https://www.blendswap.com/blend/25374
- wand https://www.blendswap.com/blend/25342

# Tiles

## Sets
- https://sketchfab.com/3d-models/polygon-dungeon-modular-floor-and-wall-tiles-3e7078284bac4c7992755f6ad93986c2

## Missing tiles
- tower
- walls for 1st and higher floors
- stairs (working & broken, small & large)
	- straight - same width & for pyramid
	- 90deg - circle & square
	- escalator (https://www.blendswap.com/blend/25589)
- bars
- fence
	- https://www.blendswap.com/blend/25394
	- barbed wire https://www.blendswap.com/blend/25391
- barriers
	- https://www.blendswap.com/blend/25393
	- https://www.blendswap.com/blend/25398
	- https://www.blendswap.com/blend/25392
	- cone https://www.blendswap.com/blend/25388
- swimming pool
- stadium (football, baseball, basketball, hockey, athletic)
	- https://www.blendswap.com/blend/25588
- cloud city https://www.blendswap.com/blend/4636

# Vegetation
- trees
	- tropical pack https://www.blendswap.com/blend/4897
	- palm https://www.blendswap.com/blend/4752
- flowers https://www.blendswap.com/blend/14532
- bush
	- https://www.blendswap.com/blend/25492

# Decorations
- tables
	- https://www.blendswap.com/blend/25352
	- https://www.blendswap.com/blend/25566
	- https://www.blendswap.com/blend/25572
- chairs
	- https://www.blendswap.com/blend/25351
	- https://www.blendswap.com/blend/25384
	- https://www.blendswap.com/blend/25404
	- https://www.blendswap.com/blend/25498
	- https://www.blendswap.com/blend/25504
	- https://www.blendswap.com/blend/25509
	- https://www.blendswap.com/blend/25573
- bench
	- park bench https://www.blendswap.com/blend/17594
- shelves
- drawers
	- https://www.blendswap.com/blend/25570
- bed
	- https://www.blendswap.com/blend/25513
- coffin https://www.blendswap.com/blend/25445
- stove
	- https://www.blendswap.com/blend/25506
- curtains
- throne
- (camp)fire
- barrel
- bin https://www.blendswap.com/blend/17587
- aquarium (https://www.blendswap.com/blend/25593)
- books
	- https://www.blendswap.com/blend/25561
- board games
	- chess https://www.blendswap.com/blend/25529
	- checkers https://www.blendswap.com/blend/25526
- tools
	- microscope https://www.blendswap.com/blend/25527
	- telescope https://www.blendswap.com/blend/25385
- electronics
	- repro
		- https://www.blendswap.com/blend/25472
		- https://www.blendswap.com/blend/25483
		- https://www.blendswap.com/blend/25478
		- https://www.blendswap.com/blend/25476
		- https://www.blendswap.com/blend/25474
		- https://www.blendswap.com/blend/25477
	- microphone https://www.blendswap.com/blend/25473
	- projector https://www.blendswap.com/blend/25417
	- megaphone https://www.blendswap.com/blend/25454
	- camera
		- polaroid https://www.blendswap.com/blend/25446
	- computer
		- https://www.blendswap.com/blend/25420
		- https://www.blendswap.com/blend/25419
		- keyboard https://www.blendswap.com/blend/25350
		- AMD Ryzen https://www.blendswap.com/blend/25424
		- cpu heatsink https://www.blendswap.com/blend/25437
		- cpu water cooling https://www.blendswap.com/blend/25438
		- cpu cooler https://www.blendswap.com/blend/25423
		- cpu heatsink & cooler https://www.blendswap.com/blend/25416
		- server
			- 24ports rack mount panel https://www.blendswap.com/blend/25496
	- mixer https://www.blendswap.com/blend/25482
- clock
	- https://www.blendswap.com/blend/25516
- sport
	- boxing bag https://www.blendswap.com/blend/25521
- knots https://www.blendswap.com/blend/25486

- car parts https://www.blendswap.com/profile/826846/blends

# Music instruments
- guitar https://www.blendswap.com/blend/25412

# Food
- bowl of rice https://www.blendswap.com/blend/25450
- Heineken https://www.blendswap.com/blend/25470