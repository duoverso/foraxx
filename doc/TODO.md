# Bugs
! MouseEvents vs. TouchEvents
! add .import files to repo
- material boxes (to Constructor level)
- Fly 2 - in direction of mouse look
- Player.max_slope_angle doesn't work

9.223.372.036.854.775.808 (max value of Godot's integer)
  283.821.900.000.000.000 (diameter of Milky Way in km)

# Optimize
- CSGMeshes to MeshInstance with StaticBody and Trimesh Collision
? apply Player.CAN_* consts to remove Input actions instead of checks under each action
- [Baked lightmaps](https://docs.godotengine.org/en/stable/tutorials/3d/baked_lightmaps.html#doc-baked-lightmaps)
- https://docs.godotengine.org/en/stable/tutorials/optimization/optimizing_3d_performance.html#culling
	There may also be rendering and physics glitches due to floating point error in large worlds. You may be able to use techniques such as orienting the world around the player (rather than the other way around), or shifting the origin periodically to keep things centred around Vector3(0, 0, 0).

# Better World
- only specified environments should be allowed for particular level
- each environment should have defined value of DirectLight.light_energy (different light multiplier for Android)
- go-through-wall

# Controls
- signal when close to enemy or quest area = collect coin, item, get access key...
- switch state with lever = open the door, teleport in current level or to another level
- unlock next level
- character properties
	- hp, energy
	- coins
	- inventory: body slots (hands, body, pants, head, rings, amulets) + backpack (with number of stackable items in slot)

	- attributes (order based on evolution)
		- strength (init 5-10, cost:1p)
		- dexterity (init 5-10, cost:1p)
		- vitality (init 10, cost:2p)
		- wisdom (cost:4p)
		- art/magic (cost:16p)
		- intelligence (cost:32p)
		? virtual attrs:
			- range: `strength*dexterity*vitality`
			- mistress: `magic*wisdom`
			- smartness: `intelligence*wisdom`

	- status bars {{}}
		- health bar (0%+ red, 25%+ yellow, 50%+ orange, 75%+ brown, 100% transparent)
			- based on vitality
		- hungry bar
			- based on health and time from eating last food
			- when hp is lower than 10% then hungry status is displayed only as border
		- energy bar
			- for running, fighting, climbing, etc... consumption based on activity, strength & dexterity
		- mana bar

	- skills

- Gravity from project's settings (move from player.gd & settings.gd)
```
    onready var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
    PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY, 9.8)
    PhysicsServer.area_set_param(get_viewport().find_world().get_space(), PhysicsServer.AREA_PARAM_GRAVITY_VECTOR, Vector3(0, -1, 0))
```
- Player.move_and_slide
	*This method should be used in Node._physics_process() (or in a method called by Node._physics_process()), as it uses the physics step's delta value automatically in calculations. Otherwise, the simulation will run at an incorrect speed.*

### Items
- item categories tree (eg. weapon/sword, consumable/potion/hp-large, resource/tree-branch)
- stackable with stack size per slot (infinite when not specified)
- title & description
- attributes:
	- weapon
	- shield
	- ring
	- amulet
	- gem slots
	? consumable
	- durability (eg. 1-100)
- random values
	- when instancing: strength +1–2, -2–-1
	- when use: eg. damage 50–100
- required player attributes to use
- weight
- throw_damage (for empty value use `weight * blunt_damage * thrown_speed*ballistic`)
- convertable
	- blue dye + yellow dye = green dye
	- in transmutation cube: weapon + shield = shielded-weapon

#### Item Categories
- weapon/
	- sword/
		- tree-branch
		- wooden-sword
- shield/
	- wooden-shield
	- bronze-shield
	- steel-shield
	- titanium-shield
- consumable/
	- potion/
		- hp-small
		- hp-large
- resource/
	- tree-branch

## TouchScreen
! disable `_input()` of joysticks for non-touchscreens (`set_process_input(false)` doesn't work :-/)
- change Buttons to TouchScreenButtons (for multitouch)
- one TouchScreenContainer overlay

## VR
! fix switching ARVR mode off
- disable TouchScreenContainer

- menu at wrist watch
- place different trees & buildings
- Fly 3 - like Descent
- select item in 3d scene with mouse and change attributes

- moving, rotating and scaling platform moves player(s) too

- Interpolation of camera switching

+ Save settings (camera 1PS, 3PS, keyboard bindings, ...)
	- Load player's position and rotation by tool in Editor
	- Change settings

+ Pause
	! Unpause in not paused part of game
- PrtScr Screenshot

- Splatmaps for terrain
- ClippedCamera (watch PlatformerDemo?)
- InterpolatedCamera
- ARVRCamera
- LOD

- AudioStream3D https://docs.godotengine.org/en/3.2/tutorials/audio/audio_streams.html
	.wav - best for short sounds, hundreds of them can be played simultaneously with little impact on performance
	. ogg - smaller files, best for music or voice in low bitrates, use considerably more CPU power to play back, even only a few at once

% use `wrapi(intVar, 1, 30)`

## AR
- GPS (https://github.com/seagsm/godot_gps)
	- https://docs.godotengine.org/en/latest/development/compiling/compiling_for_android.html

# Assets

## Materials
- bash/php script to normalize names of ALL texturehaven textures
	- displacement maps for all TextureHaven materials
	- roughness
	- ...
- create file with all materials importable to godot & blender too!
	- show names of materials in godot game

## Models
- make all terrains in blender 2.8 and easy exort workflow to godot
- seamless tiles with interpolated seams
- more buildings
- moon on the sky
- deer
- boar
- rabbit
- lightbug
- gods in the sky, world on the clouds

## Terrain
**Falloff:** 10-100 (max)

**UV unwrap:** Cube projection
	**Cube size**: 2 (1k texture per 1m)
	*or Cube size: 0.02 (1k texture per 100m to allow resize up and down in Godot from 1m to 10km)*

**Desert 1** - decimate 0.02 (tris 10,443)

### Levels
- Chauvet (https://archeologie.culture.fr/chauvet/en/explore-cave/end-chamber)
- Stonehenge
- Sistine chapel

### Maps
Czech Republic (51.0555556N,12.0913889E – 48.5525N, 18.8588889E)

Nejzápadnější bod se nachází na česko-německé hranici, v Karlovarském kraji u obce Krásná (50°15′7″ s. š., 12°5′29″ v. d.). Tento bod je též nejzápadnějším bodem bývalého Československa a nejzápadnějším bodem historické země Čechy.
Nejvýchodnější bod se nachází na česko-polské hranici, v Moravskoslezském kraji u obce Bukovec (49°33′1″ s. š., 18°51′32″ v. d.). Tento bod je též nejvýchodnějším bodem historické země České Slezsko a české části Těšínska.
Nejsevernější bod se nachází na česko-německé hranici, v Ústeckém kraji u vesnice Lobendava (51°3′20″ s. š., 14°18′58″ v. d.). Tento bod je též nejsevernějším bodem bývalého Československa a Rakousko-Uherska a nejsevernějším bodem historické země Čechy.
Nejjižnější bod se nachází na česko-rakouské hranici, v Jihočeském kraji u města Vyšší Brod (48°33′9″ s. š., 14°19′59″ v. d.). Tento bod je též nejjižnějším bodem historické země Čechy.

## Procedural in GDScript
- procedural buildings
- procedural trees
- procedural characters

# ChangeLog
+ flashlight
+ lights unified by global.tscn and removed from scenes
+ android .apk
+ change scene
+ music player
+ disable Godot's DEBUG mode in exported project
+ make material boxes
+ fix unwrapped texture in Dragon Castle
+ fix collisions in Dragon Castle tunel & cave
+ fix jump on slope floor
+ right-click to place a building