tool
extends Spatial
class_name NPC

signal player_entered
signal player_exited

export(String) var character_name : String
export(String, MULTILINE) var say : String

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Area_body_entered(body):
	if body is KinematicBody:
		emit_signal("player_entered", body)
		if say:
			HUD.dialogue("[wave]" + character_name + ":[/wave] " + say)


func _on_Area_body_exited(body):
	if body is KinematicBody:
		emit_signal("player_exited", body)
		if say:
			HUD.dialogue_close()
