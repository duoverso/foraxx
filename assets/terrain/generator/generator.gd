#tool
extends Spatial

var chunk_size := 50 # 64
var chunk_amount := 3  # 16

var last_pos:Vector3
var last_p_x:int
var last_p_z:int

var noise := OpenSimplexNoise.new()
var chunks := {}
var unready_chunks := {}

#var mutex := Mutex.new()
#var semaphore := Semaphore.new()
var thread := Thread.new()
var exit_thread := false

var update_timer:float = 100.0

#signal chunk_loaded

func _ready() -> void:
	#randomize()
	# see https://docs.godotengine.org/en/stable/classes/class_opensimplexnoise.html
	noise.seed = 3 # randi()
	# Number of OpenSimplex noise layers that are sampled to get the fractal noise. Higher values result in more detailed noise but take more time to generate.
	noise.octaves = 1  # 1-9, default: 3
	noise.period = 80

	# update_chunks() on ready
	last_pos = Global.player.translation
	last_p_x = int(last_pos.x / chunk_size)
	last_p_z = int(last_pos.x / chunk_size)
	update_chunks(last_p_x, last_p_z)
	#add_chunk(0, 0)

func _process_chunks():
	if not thread.is_active() and not unready_chunks.empty():
		var key = unready_chunks.keys()[0]
		var xz = key.split(",", true, 1)
		# warning-ignore:return_value_discarded
		thread.start(self, "load_chunk", xz, Thread.PRIORITY_LOW)

func add_chunk(x, z) -> void:
	var key := str(x) + "," + str(z)
	if chunks.has(key) or unready_chunks.has(key):
		return
	#print("add chunk: ", key)
	unready_chunks[key] = 1
	_process_chunks()

func load_chunk(xz) -> Chunk:
	var x = int(xz[0])
	var z = int(xz[1])
	#print("load_chunk(", str(xz), ")")
	#var chunk = Chunk.new(noise, x * chunk_size, z * chunk_size, chunk_size)
	var chunk = Chunk.new(noise, x, z, chunk_size)
	call_deferred("load_done", xz)
	return chunk

func load_done(xz) -> void:
	#print("load_done(", str(xz), ")")
	var chunk = thread.wait_to_finish()
	#call_deferred("emit_signal", "chunk_loaded")
	#chunk.translation = Vector3(chunk.x, 0, chunk.z)
	#chunk.visible = true
	add_child(chunk)
	var key = str(xz[0]) + "," + str(xz[1])
	chunks[key] = chunk
	# warning-ignore:return_value_discarded
	unready_chunks.erase(key)

	# Continue with next unready chunk
	_process_chunks()

func _process(delta) -> void:
	update_timer += delta
	# Optimized updating
	if update_timer > 1.0:
		# Only when Player's position changed
		if last_pos != Global.player.translation:
			last_pos = Global.player.translation
			var p_x := int(last_pos.x / chunk_size)
			var p_z := int(last_pos.z / chunk_size)
			# Only when Player's chunk position changed
			if p_x != last_p_x or p_z != last_p_z:
				print("update_chunks() :: pos.x: " + str(last_pos.x) + ", pos.z: " + str(last_pos.z) + ", p_x: " + str(p_x) + ", p_z: " + str(p_z))
				last_p_x = p_x
				last_p_z = p_z
				reset_chunks()
				update_chunks(p_x, p_z)
				clean_up_chunks()
		# Wake-up worker
		_process_chunks()
		update_timer = 0.0

# Select all chunks to be removed
func reset_chunks() -> void:
	for key in chunks:
		chunks[key].should_remove = true

func update_chunks(p_x, p_z) -> void:
	# !TODO! Generate from center to edge instead of from one edge to opposite
	# TODO: Generate circle/sphere instead of rectangle/box around player
	for x in range(p_x - chunk_amount, p_x + chunk_amount + 1):
		for z in range(p_z - chunk_amount, p_z + chunk_amount + 1):
			add_chunk(x, z)

			# Unselect chunks in range... to not be removed
			var key = str(x) + "," + str(z)
			if chunks.has(key):
				var chunk = chunks.get(key)
				if chunk != null:
					chunk.should_remove = false

# Remove all chunks still selected to be removed
func clean_up_chunks() -> void:
	for key in chunks.keys():
		var chunk = chunks.get(key)
		if chunk.should_remove:
			#print("remove chunk: ", key)
			# warning-ignore:return_value_discarded
			chunks.erase(key)
			chunk.queue_free()

func _exit_tree() -> void:
	# Stop the thread
	exit_thread = true
	# Remove all chunks
	reset_chunks()
	clean_up_chunks()
