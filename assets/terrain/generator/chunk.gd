extends Spatial
class_name Chunk
# thanks to https://www.youtube.com/watch?v=rWeQ30h25Yg
# https://github.com/nikkiluzader/godot-3d-procedural-generation

var mesh_instance
var noise
var x
var z
var chunk_size
var should_remove = false

func _init(new_noise, new_x, new_z, new_chunk_size):
	self.noise = new_noise
	self.x = new_x * new_chunk_size
	self.z = new_z * new_chunk_size
	self.chunk_size = new_chunk_size
	translation.x = self.x
	translation.z = self.z
	#visible = false
	#print("[%d, %d] (%dx%d) %s" % [self.x, self.z, self.chunk_size, self.chunk_size, self.noise])

func _ready():
	generate_chunk()
	generate_water()

func generate_chunk():
	var plane = PlaneMesh.new()
	plane.size = Vector2(chunk_size, chunk_size)
	plane.subdivide_depth = chunk_size * 0.5
	plane.subdivide_width = chunk_size * 0.5

	# Material
	#if randi() % 2 == 1:
	plane.material = preload("res://assets/materials/terrain/aerial_grass_rock/aerial_grass_rock.tres")
	#else:
		#plane.material = preload("res://assets/materials/terrain.material")

	var surface_tool = SurfaceTool.new()
	var data_tool = MeshDataTool.new()
	surface_tool.create_from(plane, 0)
	var surface = surface_tool.commit()
	# warning-ignore:unused_variable
	var error = data_tool.create_from_surface(surface, 0)

	for i in range(data_tool.get_vertex_count()):
		var vertex = data_tool.get_vertex(i)
		vertex.y = noise.get_noise_3d(vertex.x + x, vertex.y, vertex.z + z) * 50
		data_tool.set_vertex(i, vertex)

	for s in range(surface.get_surface_count()):
		surface.surface_remove(s)

	data_tool.commit_to_surface(surface)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface_tool.create_from(surface, 0)
	surface_tool.generate_normals()

	mesh_instance = MeshInstance.new()
	mesh_instance.mesh = surface_tool.commit()
	mesh_instance.create_trimesh_collision()
	mesh_instance.cast_shadow = GeometryInstance.SHADOW_CASTING_SETTING_OFF
	add_child(mesh_instance)

func generate_water():
	var plane = PlaneMesh.new()
	plane.size = Vector2(chunk_size, chunk_size)
	plane.material = preload("res://assets/materials/Water.material")

	var mesh = MeshInstance.new()
	mesh.mesh = plane

	add_child(mesh)
