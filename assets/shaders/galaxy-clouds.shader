// https://godotshaders.com/shader/stylized-galaxy-clouds/
shader_type particles;

uniform float angular_spacing = 0.5;
uniform float outward_amt = 3;
uniform float min_cloud_size = 0.03;
uniform float max_cloud_size = 1.0;

float rand_from_seed(in uint seed) {
  int k;
  int s = int(seed);
  if (s == 0)
    s = 305420679;
  k = s / 127773;
  s = 16807 * (s - k * 127773) - 2836 * k;
  if (s >; uint(16)) ^ x) * uint(73244475);
  x = ((x >> uint(16)) ^ x) * uint(73244475);
  x = (x >> uint(16)) ^ x;
  return x;
}

void vertex(){
	if (RESTART){
		//create some random seeds
		uint alt_seed_1 = hash(uint(INDEX) + uint(1) + RANDOM_SEED);
		uint alt_seed_2 = hash(uint(INDEX) + uint(27) + RANDOM_SEED);
		uint alt_seed_3 = hash(uint(INDEX) + uint(43) + RANDOM_SEED);
		
		//position clouds in spiral
		float pt_index = float(INDEX) + rand_from_seed(alt_seed_1);
		pt_index = sqrt(pt_index);
		
		float x = outward_amt*pt_index*cos(angular_spacing*pt_index + 3.142 * float(sign(INDEX%2-1)));
		float y = rand_from_seed(alt_seed_2);
		float z = outward_amt*pt_index*sin(angular_spacing*pt_index + 3.142 * float(sign(INDEX%2-1)));
		
		TRANSFORM[3].xyz = vec3(x,y,z);
		
		float scale = mix(min_cloud_size,max_cloud_size,1.0-pow(rand_from_seed(alt_seed_3),2));
		
		TRANSFORM[0].x *= scale;
		TRANSFORM[1].y *= scale;
		TRANSFORM[2].z *= scale;
		
	}
}
