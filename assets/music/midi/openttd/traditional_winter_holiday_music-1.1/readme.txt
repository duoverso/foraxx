TRADITIONAL WINTER HOLIDAY MUSIC PACK 
Version 1.1
Released November 27, 2013 by Kevin Fields (kamnet) 

RELEASE NOTES 
------------------------------------------------------------------------------
It has taken me nearly four years to assemble this pack, but through great
persistence I have assembled, what I believe to be, some of the greatest 
sequenced MIDI files of traditional winter holiday music. I have taken much 
time and great care to select these pieces not only for their popularity, 
but for the unique, polysymphonic sound quality that they were sequenced. 
It has also taken much time to contact the sequencers of these pieces and 
obtain permission for their distribution. In some cases, the original 
sequencers have passed on, and the care for their works have passed on to 
family members or other loved ones who also share in the passion of these 
works. This music pack is not only a tribute to some of our most cherished 
holiday songs as the centuries have gone by, but also to the individuals who 
took the time to preserve the spirit and memory of the song. MIDI sequencers 
are a diverse, talented, and caring group of individuals who have dedicated 
themselves to the task of showing only the best quality of this form of music.

PLAYLIST AND CREDITS
------------------------------------------------------------------------------
1:22 01. "We Wish You a Merry Christmas" 
         Sequenced by Bill Basham, http://divtune.com/ 
1:19 02. "The Holly and the Ivy" 
         Sequenced by Bob Sorem, http://www.tc.umn.edu/~sorem002/xmas_midi.html
4:02 03. "The Twelve Days of Christmas" 
         Sequenced by David Schmid, http://www.musicshoppe.com/
1:26 04. "Up on the Housetop" 
         Sequenced by Bill Basham, http://divtune.com/ 
2:41 05. "The Gloucesteshire Wassail" 
         Sequenced by George Tassara, http://www.tassara.net/
2:19 06. "Still, Still, Still" 
         Sequenced by Bob Sorem, http://www.tc.umn.edu/~sorem002/xmas_midi.html
4:15 07. "All Through the Night 
         Sequenced by Mike Magatagan, http://musescore.com/mike_magatagan/
2:24 08. "Jingle Bells" 
         Sequenced by Bob Barnes, http://www.ajsmidi.com/barnes/barnes_1.html
3:36 09. "Jolly Old St. Nicholas" 
         Sequenced by "Perfessor" Bill Edwards - http://www.perfessorbill.com/
1:04 10. "Boar's Head Carol" 
         Sequenced by Bob Sorem, http://www.tc.umn.edu/~sorem002/xmas_midi.html
1:50 11. "Good King Wencelas"
         Sequenced by Melvin Webb, http://www.kmelmidimusic.com/
3:14 12. "The Huron Carol" 
         Sequenced by Barry Taylor, "The Great Canadian Tunebook", http://members.shaw.ca/tunebook/index.htm
1:05 13. "O Tannebaum" 
         Sequenced by Bob Sorem, http://www.tc.umn.edu/~sorem002/xmas_midi.html
3:18 14. "Reindeer Ragtime" 
         Sequenced by "Perfessor" Bill Edwards - http://www.perfessorbill.com/ 
1:40 15. "Parade of the Wooden Soldiers" 
         Sequenced by Donald F. Kornegay, http://web.archive.org/web/20080719165728/http://members.aol.com/kornegayd/
2:10 16. "Joy to the World" 
         Sequenced by Eleanor Adams, http://web.archive.org/web/20010111204900/http://www.midihaven.addr.com/midi/eleanor1.html
3:28 17. "Away in a Manger"
         Sequenced by Harry "Gitpicker" Todd, http://gitpicker5.tripod.com/ 
3:03 18. "Carol of the Bells" 
         Sequenced by David Schmid, http://www.musicshoppe.com/
3:45 19. "Auld Lang Syne"
         Sequenced by Melvin Webb, http://www.kmelmidimusic.com/
1:25 20. "Toyland" 
         Sequenced by Donald F. Kornegay, http://web.archive.org/web/20080719165728/http://members.aol.com/kornegayd/

		 
HOW TO INSTALL
------------------------------------------------------------------------------
The easiest method to install this base music pack is through the OpenTTD 
in-game content download system, where it is listed under "Base Music" as 
"Traditional Winter Holiday Muisc Pack". This will automatically download 
the pack to the proper directory. 

MANUAL INSTALLATION FOR OPENTTD 1.2.0 AND LATER
Unpack the folder "Traditional_Winter_Holiday_Music" to the /baseset 
subdirectory where you installed OpenTTD.

MANUAL INSTALLATION FOR OPENTTD 1.1.5 AND EARLIER 
Unpack the folder "Traditional_Winter_Holiday_Music" to the /gm 
subdirectory where you installed OpenTTD.
