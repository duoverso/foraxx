MODER MOTION MUSIC PACK 
Version 1
Released November 29, 2013 by Kevin Fields (kamnet) 

RELEASE NOTES 
------------------------------------------------------------------------------
The Modern Motion Music Pack pays tribute to the spirit of the original music 
from Transport Tycoon Deluxe and composer John Broomhall. Running nearly 99 
minutes long, these 30 songs bring a modern sound to OpenTTD, and should 
inspire gameplay for games in the late 20th and early 21st century. All songs 
here are original compositions from Kit "Monochrome" F, George E. Martin, 
Bjorn Lynne and Brian Dill. 

All works are copyright by the original artists and released to players of 
OpenTTD under a Creative Commons license. Users may distribute this original 
collection with proper attribution to all artists, it may not be used for 
commercial purposes, and you may not distribute any modified or derivative 
versions of these works. This release is intended solely for your own personal 
use and enjoyment. For full license and details, please visit 
http://creativecommons.org/licenses/by-nc-nd/3.0/us/ or refer to the file 
"license.txt" in this set. 


PLAYLIST AND CREDITS
------------------------------------------------------------------------------
01. 1:57 Rock Force
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
02. 3:35 The Angry Pawn
    Sequenced by Troy S. Carlson, http://www.justmidis.com/All-Artists/Troy-S-Carlson-Midis.shtml
03. 5:26 City of Energy
    Sequenced by Kit "Monochrome" F, http://www.justmidis.com/Original/Midi-Music/Kit--Monochrome--F-Original-Midis.shtml
04. 2:59 Walk the Dog
    Sequenced by Brian Dill, http://www.leftandwrite.com/brian/music/midi_songs.php
05. 4:33 Rockin' Ruby
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
06. 3:45 Streetlights and Fences
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
07. 4:30 Wait for Me
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
08. 3:18 Hometown
    Sequenced by Kit "Monochrome" F, http://www.justmidis.com/Original/Midi-Music/Kit--Monochrome--F-Original-Midis.shtml
09. 1:20 The Late One
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
10. 4:18 Heavier Things
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
11. 2:36 Open Pod
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
12. 2:40 Heavy Beat
    Sequenced by Brian Dill, http://www.leftandwrite.com/brian/music/midi_songs.php
13. 4:45 Off Balance
    Sequenced by Kit "Monochrome" F, http://www.justmidis.com/Original/Midi-Music/Kit--Monochrome--F-Original-Midis.shtml
14. 3:20 Let's Go
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
15. 3:26 Green Hill
    Sequenced by Kit "Monochrome" F, http://www.justmidis.com/Original/Midi-Music/Kit--Monochrome--F-Original-Midis.shtml
16. 3:29 Funky Groove
    Sequenced by Brian Dill, http://www.leftandwrite.com/brian/music/midi_songs.php
17. 2:50 Flying Colours
    Sequenced by Troy S. Carlson, http://www.justmidis.com/All-Artists/Troy-S-Carlson-Midis.shtml
18. 4:09 Understatement
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
19. 4:15 Modern Problems
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
20. 1:39 Techno1 
    Sequenced by Brian Dill, http://www.leftandwrite.com/brian/music/midi_songs.php
21. 2:45 The Beheading of King Charles I
    Sequenced by Troy S. Carlson, http://www.justmidis.com/All-Artists/Troy-S-Carlson-Midis.shtml
22. 4:50 All of Us
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
23. 2:29 Winding Roads
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
24. 4:20 Tea Garden
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
25. 2:02 Back Alley
    Sequenced by Brian Dill, http://www.leftandwrite.com/brian/music/midi_songs.php
26. 4:49 Hot Pink
    Sequenced by George E. Martin, http://www.justmidis.com/Original/Midi-Music/George-E-Martin-Original-Midis.shtml
27. 2:26 Borgan Lues
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
28. 1:57 Jaywalkin' 
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
29. 1:05 Chill Cafe
    Sequenced by Bjorn Lynne (PRS), http://www.Shockwave-Sound.com
30. 2:16 Farewell Address
    Sequenced by Troy S. Carlson, http://www.justmidis.com/All-Artists/Troy-S-Carlson-Midis.shtml

		 
HOW TO INSTALL
------------------------------------------------------------------------------
The easiest method to install this base music pack is through the OpenTTD 
in-game content download system, where it is listed under "Base Music" as 
"Traditional Winter Holiday Music Pack". This will automatically download 
the pack to the proper directory. 

MANUAL INSTALLATION FOR OPENTTD 1.2.0 AND LATER
Unpack the folder "Traditional_Winter_Holiday_Music" to the /baseset 
subdirectory where you installed OpenTTD.

MANUAL INSTALLATION FOR OPENTTD 1.1.5 AND EARLIER 
Unpack the folder "Traditional_Winter_Holiday_Music" to the /gm 
subdirectory where you installed OpenTTD.
