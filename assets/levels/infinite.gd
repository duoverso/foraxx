extends Level

func _on_ToCanyon_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("canyon")

func _on_ToCanyons_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("canyons")

func _on_ToDesert_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("desert")

func _on_ToFlatstones_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("flatstones")
