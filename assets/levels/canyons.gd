extends Level

func _on_ToDesert_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("desert", Vector3(100, 10, 10))
