extends Level

func _on_Area_body_exited(body):
	if body is KinematicBody:
		HUD.dialogue_close()
		HUD.dialogue_big_close()
		HUD.hide_event_text()

func _on_AreaUnderground_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("\n[b][wave amp=125 freq=2]You felt over the Edge.[/wave][/b]\n\nUse [b]BACKSPACE[/b] to get back to start.")

func _on_AreaVendingMachine_body_entered(body):
	if body is KinematicBody:
		LevelEnvironment.rand_environment()
		HUD.dialogue("Press [b]H[/b] to toggle environments randomly.")

func _on_Area_Control_Desk_body_entered(body):
	if body is KinematicBody:
		$Generator.noise.seed = randi()
		HUD.set_event_text("Pattern of The Ground was changed to " + str($Generator.noise.seed))

func _on_AreaReturn_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Teleport of the Return[/wave]\n\nUse BACKSPACE to return back to level start.")

func _on_Return_body_entered(body):
	if body is KinematicBody:
		#LevelManager.player_return_to_start()
		Global.player.translation = Vector3(0, 100, 0)

func _on_AreaLevelThis_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Teleport of the Lobby[/wave]\n\nUse L to return back to this Lobby level.")

func _on_ToThisLevel_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("constructor", Vector3(0, 10, 0))
	pass

#func _on_AreaLevelCanyon_body_entered(body):
#	if body is KinematicBody:
#		HUD.dialogue("[wave]Crystal Teleport to the Canyon[/wave]\n\n[b]UNDER CONSTRUCTION[/b]")

func _on_ToCanyon_body_entered(body):
	if body is KinematicBody:
		#LevelManager.goto_level("canyon")
		$Teleports/Portal_Canyon.horizont_toggle()

#func _on_AreaLevelCanyons_body_entered(body):
#	if body is KinematicBody:
#		HUD.dialogue("[wave]Crystal Teleport to the Canyons[/wave]\n\n[b]UNDER CONSTRUCTION[/b]")

func _on_ToCanyons_body_entered(body):
	if body is KinematicBody:
		#LevelManager.goto_level("canyons")
		$Teleports/Portal_Canyons.horizont_toggle()

#func _on_AreaLevelDesert_body_entered(body):
#	if body is KinematicBody:
#		#HUD.dialogue("[wave]Crystal Teleport to the Desert[/wave]\n\nUse B and N key to change levels (Back and Next).")

func _on_ToDesert_body_entered(body):
	if body is KinematicBody:
		$Teleports/Portal_Desert.horizont_toggle()
		#LevelManager.goto_level("desert")

func _on_ToFlatstones_body_entered(body):
	if body is KinematicBody:
		#LevelManager.goto_level("flatstones")
		#$Teleports/Portal_Flatstones.set_horizont_active() = true
		$Teleports/Portal_Flatstones.horizont_toggle()

func _on_AreaLevelHills_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Teleport to the Hills[/wave]\n\n[b]UNDER CONSTRUCTION[/b]")

func _on_ToHills_body_entered(body):
	if body is KinematicBody:
		$Teleports/Portal_Hills.horizont_toggle()

func _on_AreaLevelRiverland_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Teleport to the Riverland[/wave]\n\n[b]UNDER CONSTRUCTION[/b]")

func _on_ToRiverLand_body_entered(body):
	if body is KinematicBody:
		$Teleports/Portal_RiverLand.horizont_toggle()
		#LevelManager.goto_level("dragons")

func _on_AreaLevelInfinite_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Teleport to The Infinite[/wave]\n\n[b]UNDER CONSTRUCTION[/b]")

func _on_ToInfinite_body_entered(body):
	if body is KinematicBody:
		LevelManager.goto_level("infinite")

func _on_AreaRollercoasterPF2020_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("\n[center][wave amp=100 freq=5][tornado radius=20][rainbow]Happy New Year 2021[/rainbow][/tornado][/wave][/center]\n\nRollercoaster Crystal will show you the village.\n[b]Use F5[/b] to change view back from rollercoaster.")

func _on_RollercoasterPF2020_body_entered(body):
	if body is KinematicBody:
		HUD.hide_event_text()
		HUD.dialogue_close()
		HUD.dialogue_big_close()
		#$RollercoasterPF2020/Path/Camera.current
		Global.player.cam.current = false
		$Teleports/RollercoasterPF2020/Path/Camera.current = true
		$Teleports/RollercoasterPF2020/Path/Camera.set_process(true)

func _on_AreaBloater_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Bloater[/wave]: \"You wanna fight?! Just wait for that...\"")

func _on_AreaCat_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Cat[/wave]: \"Meow!\"\n\nUse . (dot key) to stop/play music or , (period key) to play next track.")
		# warning-ignore:return_value_discarded
		Music.play_next()

func _on_AreaCrystalDragon_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Crystal Dragon: \"Welcome at The Sky.\"[/wave]\n\nTrain more and who knows...\n\n... maybe we can fight together one day 🐲")

func _on_AreaDragonSmall_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Little Dragon[/wave]: \"Press TAB to switch to Fly with Keyboard.\"\n\nWASD + C|E (fly down) + Space (fly up)\nPress Shift+Tab to switch to Fly with Mouse mode.")

func _on_AreaInvisibleWoman_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Invisible Woman[/wave]: \"How are you? \"\n\nHold [ or ] to change distance of fog.\nToggle fog with F10.")

func _on_AreaIonian_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Ionian: \"Show me your Debug info with F12 key.\"[/wave]")

func _on_AreaIonian2_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Ionian2: \"Just continue. Or you have something better to do?\"[/wave]")

func _on_AreaIonian3_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Ionian3[/wave]: \"You want what?? How to learn the art of Fight? :-D ha ha\n\nIt's not so simple. Look for Fire Crystals over here.\"")

func _on_AreaIronman_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Ironman[/wave]: \"Welcome in ACV [i](Asgard's Construction Village)[/i].\"\n\nUse scroll wheel to zoom, / (slash key) to reset FOV.\n\nDematerialization field responsible for teleports looks identical with toroidal fields around.")

func _on_AreaJixie_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Jixie[/wave]: \"Welcome\"\n\nLeft-click to seed the tree :-O\n\nRight-click to build the house (-8")

func _on_AreaNeo_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Neo[/wave]: \"Welcome out of the Matrix...\n\n.Just hold +/PLUS to increase or -/MINUS to decrease the speed.\nAnd you can even shrink yourself with Ctrl+MINUS or enlarge with Ctrl+PLUS.")
		Global.player.speed = 1000

func _on_AreaQueenRutela_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Queen Rutela[/wave]: \"Welcome in The Heaven.\n\nJump with SPACE key. You will jump higher with higher speed.\"")

func _on_AreaRangikuChristmas_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Rangiku[/wave]: \"Be careful! If you fall over edge then use BACKSPACE to get back to the start!\"")

func _on_AreaSilverSurfer_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Silver Surfer[/wave]: \"Here are our vehicles.\nUnfortunately you can't control them yet.\nAnd no, I won't lend you my surfboard.\nMaybe later...")

func _on_AreaSnoke_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Snoke[/wave]: \"Welcome in the Lobby level.\n\nWhen you're out of here, you can use L key to return back to the Lobby.\"")

func _on_AreaSuccubus_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Succubus[/wave]: \"Fortunately for you i saved your position!\"\n\nSave your position in current level by F2, load it by F3.")
		Settings.save()

func _on_AreaYoruichi_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Yoruichi[/wave]: [b]\"Welcome in the village.\"[/b]\n\nUse [b]SHIFT[/b] for double speed.\nUse numeric keys 1-7 to change speed.\n[b]1[/b] walk       [b]2[/b] run       [b]3[/b] ride       [b]4[/b], [b]5[/b] fly with [i]Shift+[/i][b]TAB[/b]       [b]6[/b], [b]7[/b] cosmic speeds")
		Global.player.speed = 1000

func _on_AreaGateGuard_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue("[wave]Gates Guard[/wave]: [b]\"I opened three portals here for you.\"[/b]\n\nChoose wisely...")
		$Teleports/Portal_Flatstones.set_horizont_active(true)
		$Teleports/Portal_Canyon.set_horizont_active(true)
		$Teleports/Portal_Canyons.set_horizont_active(true)

func _on_AreaInfoFight_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue_big("[wave]Crystal of Fire[/wave]\n\n[wave amp=150 freq=3][b]Sorry, it's UNDER CONSTRUCTION.[/b][/wave]\n\nLeft-click : primary attack (place a tree :-)\nRight-click : secondary action (build a house)\n\n@TODO CTRL+1, CTRL+2 to change weapons")

func _on_AreaBBCode_body_entered(body):
	if body is KinematicBody:
		HUD.dialogue_big("""[wave]Crystal of BBcode[/wave]

[b]b (bold)[/b]
[i]i (italics)[/i]
[u]u (underline)[/u]
[s]s (strikethrough)[/s]
[code]code[/code]
[color=red]color=name/code[/color]
[font]font=<path>[/font]

[center]center[/center]
[right]right[/right]

[b]Table:[/b]
[table]
[cell]cell 1[/cell]
[cell]cell 2[/cell]
[/table]

[fill]fill[/fill]

[indent]indent[/indent]

[url]https://duoverso.com[/url]

[wave]wave amp=50 freq=2[/wave]

[tornado]tornado radius=5 freq=2[/tornado]

[shake]shake rate=5 level=10[/shake]

[fade]fade start=4 length=14[/fade]

[rainbow]rainbow freq=0.2 sat=10 val=20[/rainbow]

[b]Custom:[/b]

[ghost]ghost freq=5.0 span=10.0[/ghost]

[matrix]matrix clean=2.0 dirty=1.0 span=50[/matrix]

[pulse]pulse color=#00FFAA height=0.0 freq=2.0[/pulse]""")

