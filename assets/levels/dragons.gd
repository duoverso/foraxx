extends Level

func _on_GainAbility_Jump_Crouch_body_entered(body):
	print("Body gain jump&crouch: ", body.get_class())
	if body is Player:
		body.abilities |= Player.CAN_JUMP | Player.CAN_CROUCH

func _on_GainAbility_Primary_body_entered(body):
	print("Body gain primary: ", body.get_class())
	if body is Player:
		body.abilities |= Player.CAN_USE_PRIMARY

func _on_GainAbility_Secondary_body_entered(body):
	print("Body gain secondary: ", body.get_class())
	if body is Player:
		body.abilities |= Player.CAN_USE_SECONDARY

func _on_GainAbility_Fly_body_entered(body):
	print("Body gain fly: ", body.get_class())
	if body is Player:
		body.abilities |= Player.CAN_FLY
