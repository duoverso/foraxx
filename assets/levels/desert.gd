extends Level

#onready var materials = [
	#$"Desert".mesh.surface_get_material(0),
	# Terrain
	#$"../Platform".material,
	#$"../Terrain/Canyon".mesh.surface_get_material(0),
	#$"../Terrain/Canyons".mesh.surface_get_material(0),
	#$"../Terrain/Desert".mesh.surface_get_material(0),
	#$"../Terrain/Riverland".mesh.surface_get_material(0),
	#$"../Terrain/Flatstones".mesh.surface_get_material(0),
	#$"../Terrain/Lakes1/Lakes1".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/DragonCastle".mesh.surface_get_material(0),
	#$"../Terrain/DragonCastle/Dungeon".mesh.surface_get_material(0),
	#$"../Terrain/Hills".mesh.surface_get_material(0),
	# Animals
	#$"../Animals/DragonTutu/Armature/Skeleton/Tutu".mesh.surface_get_material(0),
#]

func _ready():
	#$ToMatrix.connect("body_entered", Global.player, "_on_player_entered_into_desert_box")
	#if not $ToMatrix.is_connected("body_entered", self, "_on_Area_body_entered"):
	#	$ToMatrix.connect("body_entered", self, "_on_Area_body_entered")
	pass

func _on_ToConstructor_body_entered(body):
	#print("Desert: body entered ToConstructor")
	#print_debug(body)
	if body is KinematicBody:
		LevelManager.goto_level("constructor")


func _on_Area_body_entered(body):
	if body is Player:
		body.abilities |= Player.CAN_JUMP | Player.CAN_CROUCH
