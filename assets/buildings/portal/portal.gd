### TODO
# more generic class with only show_in_editor and calling all setters
tool
extends Node

signal horizont_activated
signal horizont_deactivated
signal player_entered

export(bool) var show_in_editor := true setget set_show_in_editor, get_show_in_editor
export(Material) var portal_material : Material setget set_portal_material, get_portal_material
export(Color) var portal_color : Color setget set_portal_color, get_portal_color
export(bool) var horizont_active : bool setget set_horizont_active, get_horizont_active
export(Material) var horizont_material : Material setget set_horizont_material, get_horizont_material
export(Color) var horizont_color : Color setget set_horizont_color, get_horizont_color
export(String) var teleport_to : String
export(String, MULTILINE) var dialogue_text : String
# selectable Interaction
#export(Interaction) var interaction

#func _enter_tree():
	#print(name, "._enter_tree()")
	#horizont_color = Color(randf(), randf(), randf())
	#set_horizont_color(horizont_color)
	#portal_color = Color(randf(), randf(), randf(), 0.5)
	#set_portal_color(portal_color)

func _init() -> void:
	#print(name, "._init(): ", _import_path)
	#if name == "":
	#	for i in get_property_list():
	#		print("  ", i.name)

	if is_inside_tree():
		print("is_inside_tree")
		if get_tree().current_scene:
			print("current_scene: ", get_tree().current_scene, " (", get_tree().current_scene.filename, ")")
			#print(get_tree().edited_scene_root.filename)
	#print(get_node("/root").name)
	#if (get_tree() != null):
	#randomize()
	#material_override = mesh.surface_get_material(0).duplicate() # DUPLICATE_USE_INSTANCING)
	#material_override.albedo_color = color
	#print(name, "._init(): ", material_override, ", color: ", color)
	#name = "asdf"

func _ready() -> void:
	#print(name, "._ready()")
	pass # Replace with function body.

func get_show_in_editor() -> bool:
	return show_in_editor

func set_show_in_editor(new_value : bool) -> void:
	show_in_editor = new_value
	if Engine.is_editor_hint():
		if new_value:
			set_portal_material(portal_material)
		else:
			if $Portal6.material_override:
				$Portal6.material_override = null

func get_portal_material() -> Material:
	return portal_material

func set_portal_material(new_material : Material) -> void:
	portal_material = new_material
	#if Engine.is_editor_hint():
	if has_node("Portal6"):
		var portal := $Portal6
		portal.material_override = new_material

func get_portal_color():
	#print(name, ".get_portal_color()")
	return portal_color

func set_portal_color(new_color : Color) -> void:
	portal_color = new_color
	#print(name, ".set_horizont_color()")
	#if Engine.is_editor_hint():
		#print("set_color")
		#if material_override:
		#	material_override.free()
		#if !material_override:
	if has_node("Portal6"):
		var portal := $Portal6
		# Duplicate material before change color (existing material_override or material/1 from mesh)
		if portal.material_override:
			portal.material_override = portal.material_override.duplicate()
		else:
			portal.material_override = portal.mesh.surface_get_material(0).duplicate() #DUPLICATE_USE_INSTANCING)
		#print(name, ": Material overriden, ", mesh.surface_get_material(0))
		if portal.material_override is SpatialMaterial:
			portal.material_override.albedo_color = new_color
		#EditorInterface.get_resource_previewer()
		#EditorPlugin.get_editor_interface().get_resource_previewer()

func get_horizont_active() -> bool:
	return horizont_active

func set_horizont_active(activate : bool) -> void:
	horizont_active = activate
	if has_node("Horizont"):
		$Horizont.visible = activate
	if activate:
		emit_signal("horizont_activated")
	else:
		emit_signal("horizont_deactivated")

func horizont_toggle() -> void:
	set_horizont_active(!horizont_active)

func get_horizont_material() -> Material:
	return horizont_material

func set_horizont_material(new_material : Material) -> void:
	horizont_material = new_material
	#if Engine.is_editor_hint():
	if has_node("Horizont"):
		var horizont = $Horizont
		horizont.material_override = new_material

func get_horizont_color() -> Color:
	#print(name, ".get_horizont_color")
	#for i in $Horizont.get_property_list():
	#	print(i.name)
	#print($Horizont.get_surface_material(0))
	#print(mesh.surface_get_material(0))
	return horizont_color

func set_horizont_color(new_color : Color) -> void:
	horizont_color = new_color
	#print(name, ".set_portal_color()")
	#if Engine.is_editor_hint():
	if has_node("Horizont"):
		var horizont = $Horizont
		# Duplicate material before change color (existing material_override or material/1 from mesh)
		if horizont.material_override:
			horizont.material_override = horizont.material_override.duplicate()
		else:
			horizont.material_override = horizont.mesh.surface_get_material(0).duplicate()
		if horizont.material_override is SpatialMaterial:
			horizont.material_override.albedo_color = new_color

func _on_HorizontArea_body_entered(body) -> void:
	if body is KinematicBody:
		if horizont_active:
			emit_signal("player_entered")
			if teleport_to:
				LevelManager.goto_level(teleport_to)
			else:
				HUD.set_event_text("This leads to NOWHERE :-(")
		else:
			HUD.set_event_text("It's currently not active")

func _on_HorizontArea_body_exited(body) -> void:
	if body is KinematicBody:
		HUD.hide_event_text()

func _on_PortalArea_body_entered(body) -> void:
	if body is KinematicBody and dialogue_text:
		if horizont_active:
			HUD.dialogue(dialogue_text)

func _on_PortalArea_body_exited(body) -> void:
	if body is KinematicBody and dialogue_text:
		HUD.dialogue_close()
