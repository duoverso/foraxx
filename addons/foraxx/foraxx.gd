# https://docs.godotengine.org/en/stable/tutorials/plugins/editor/making_main_screen_plugins.html
tool
extends EditorPlugin

var screen
var dock

func _enter_tree():
	# Create main screen
	#get_tree().set_meta("__editor_interface", get_editor_interface())
	screen = preload("res://addons/foraxx/screen.tscn").instance()
	get_editor_interface().get_editor_viewport().add_child(screen)
	make_visible(false)
	# Create dock
	#dock = preload("res://addons/foraxx/dock.tscn").instance()
	#add_control_to_dock(DOCK_SLOT_LEFT_UL, dock)

func _exit_tree():
	# Remove screen
	get_editor_interface().get_editor_viewport().remove_child(screen)
	# Remove dock
	#remove_control_from_docks(dock)

func has_main_screen():
	return true

func make_visible(visible):
	if screen:
		screen.visible = visible

func get_plugin_name():
	return "Foraxx"

func get_plugin_icon():
	# See icons at https://github.com/godotengine/godot/tree/master/editor/icons
	return get_editor_interface().get_base_control().get_icon("WorldEnvironment", "EditorIcons")
